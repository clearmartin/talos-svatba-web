# JÁDRO DOKUMENTACE KE SVATEBNÍ HŘE

Tady budou základní věci, rozcestníky do případně dalších dokumentů apod., které potřebujeme k vytvoření svatební hry.

Celkově musíme vymyslet <tento počet> věcí:
-> Digitální technické zpracování = terminál, aliasy pro příkazy, event. zrušení "nebezpečných" příkazů, QR kódy, atd.
-> Fyzické technické zpracování = propojení digitálních informací s věcmi v realitě. Třeba krabička s něčím, stránka v knize,
první písmena nějaké věty nebo na stránce, prostě takové fyzické úkoly.
-> Příběhy = taková ta "omáčka" okolo!
-> 

-----

## ROZCESTNÍK

- Terminal --> terminal.md
- Hry, puzzly, úkoly apod.  --> interactions.md
- Příběhy, vysvětlení, další texty atd.  --> stories.md

-----
