# TECHNICKÉ ZPRACOVÁNÍ TERMINÁLU PRO NAŠI SVATEBNÍ HRU

## Terminálové aliasy a skripty

- home skript: hotový, udělá 'clear' a napíše úvodní text.
- open skript: relativně hotový, ale pozor! Přímo tady není aktuální verze.
Ta správná aktuální verze používá Martinův `open_in_subwindow.py` skript,
který je jednak ve složce `bin/`, ale taky jsem ho přidal tady do `.bin/`.
- list alias: udělá 'tree -L 3', to mi přijde hezké a praktické.
- help alias: Tohle ještě potřebuje práci. Každopádně to bude alias pro nápovědu
k nejrůznějším použitím toho terminálu. Typicky by tam mělo být napsané, že pokud
je soubor ve složce, tak můžeš buď udělat 'cd' nebo ho můžeš rovnou otevřít pomocí
'open' a napsat nejprve složku a pak soubor.

- barevné echo: pomocí příkazu podobného tomuhle
`echo -e " This is\e[1;35m purple\e[0m text"`
lze dělat barevný echo output! To se bude hrozně hodit, takže jestli to jen trochu
půjde, měl bych to udělat. Jak pro nápovědy ke hře (jako modrá/červená krabička a tak),
tak i pro odlišení významných částí textu - např. textů s dalšími rozhodnutími pro
pokračování příběhu.

Článek k tomu:
https://www.oreilly.com/library/view/linux-shell-scripting/9781785881985/b0ddd805-aa79-441d-b5a7-380c66c7712d.xhtml

-----

## Zobrazování textů ve hře

Co se týče hry samotné, tak se mi zdá jako nejjednodušší prostě napsat každé entry
do samostatného bash skriptu, a pak tomu skriptu udělat odpovídající alias.

Psát to do aliasů přímo by byla hrozná otrava a snadno by se v tom udělala chyba,
kdežto ve skriptu to bude relativně bezpečnější a navíc jednodušší. A taky přehlednější.
Takže až budu mít texty ve složce /story/ hotové, tak je potřeba je přepsat do skriptů,
následně udělat aliasy a pak to ještě všechno přesunout na ty systémy.

Jako neni to málo práce! :/

-----

## Zobrazování obrázků

Podle všeho by měl fungovat prográmek `feh`! Musím ještě vyzkoušet v headless prostředí.
Ale zatím to vypadá nadějně.
Jenom by k tomu bylo pak potřeba dát nějaký návod, aby to lidi fakt dokázali používat.

**Nastavení xdg-open**
Jinak, pro nastavení v 'xdg-open' u obrázků je potřeba použít 'image/png' a 'image/jpeg'...

**Options ve fehu**
-x = borderless, 
-G = draw actions in-window, 
-F = Fullscreen,
-. = scale down images to fit geometry

více v 'man feh'

**EDIT: FEH ASI NENÍ OPTIMÁLNÍ - SPÍŠ BUDEME MUSET POUŽÍVAT FRAMEBUFFER A `fbi`.**
**Což by neměl být v zásadě problém, ale chce to ještě pro jistotu dobře otestovat.**

-----

## Zobrazování dokumentů

Zatím používám 'less'... Popravdě mi to přijde v pohodě, ale samozřejmě
když najdeme něco lepšího, tak proč ne, žejo. :)

Martin napsal skript `open_in_subwindow.py`, který by tohle měl hezky řešit. Už jsme ho
i zkoušeli, tak věřím že to bude funkční.
Jediné co nedělá zatím správně, je řádkování - to musíme vychytat. A taky bude asi potřeba
to zobrazovat přes `tmux`, protože bez něj to na těch headless systémech jenom překreslí
celou obrazovku, ale nevypne se to doopravdy, potom co vypnu to okno. Ale tmux by to prý měl
řešit.

-----

## Zobrazování videí

Našel jsem utilitku 'omxplayer', která je specificky pro Raspi (ale snad půjde kdyžtak
vybuildit) a slouží k zobrazování videí, pokud to chápu správně tak přes framebuffer.

Další možnosti jsou teoreticky tady, ale zatím je nemám tak dobře vyzkoušené:
https://askubuntu.com/questions/46871/how-can-i-play-videos-in-a-framebuffer

Tohle je spíš nice-to-have, videa jsou možná trochu overkill pro ta slaboučká Raspberry Pi.

-----
