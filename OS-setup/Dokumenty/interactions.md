# DOKUMENTACE K INTERAKTIVNÍM PRVKŮM NAŠÍ SVATEBNÍ HRY

## Digitální prvky


-----

## Fyzické prvky

- Najít nějakou zajímavou knížku v knihovně, dát ji někam a říct lidem,
ať hledají nějakou stránku, z ní vezmou některá písmena a z nich se 
jim poskládá zpráva.

- Cílem je něco najít, indicie je fotka nebo něco, byla by hádanka,
odpověď na ni by byla "kov" a šlo by o kovovou krabičku, ve které
je něco zásadního.

-----

Obecně všechno tohle mám už líp popsaný ve složce ../../story/, takže
spíš už koukejme tam.
