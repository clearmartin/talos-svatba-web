BIN_HOME="$HOME/.project/talos-svatba-web/OS-setup/.bin"

alias help='$BIN_HOME/help.sh'
alias nápověda='$BIN_HOME/help.sh'

alias domů='cd ~/ && $BIN_HOME/home.sh'
alias home='cd ~/ && $BIN_HOME/home.sh'

# alias list='tree -L 3'

alias open='$BIN_HOME/open.sh'
alias otevři='$BIN_HOME/open.sh'
