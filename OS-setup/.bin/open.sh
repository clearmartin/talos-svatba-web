#!/bin/bash

Filetype=`xdg-mime query filetype $1`
echo "Filetype: " $Filetype
if [[ "$Filetype" == "image/"* ]]; then
    echo "Opening picture" $1
# toto aktualizovat na fbi při použití na bare-metalu!
    feh $1
elif [[ "$Filetype" == "text/"* ]]; then
    echo "Opening document" $1
    .bin/open_in_subwindow.py $1
else
    xdg-open $1
fi
echo "OK."
