#!/usr/bin/env python3

import os, shutil, random

home_dir=os.path.dirname(os.path.realpath(__file__)) + '/..'

os.chdir(home_dir)

story_file_path=home_dir + '/../story/dialogs.md'
puzzles_file_path=home_dir + '/../story/puzzles.md'
articles_path=home_dir + '/articles'

shutil.rmtree(articles_path)
os.makedirs(articles_path)

class Article:
    def __init__(self):
        self.name = ''
        self.command = ''
        self.text_lines = []
        self.text_lines_in_window = []
        self.alias_of = ''
        self.encrypted = False
        self.password = ''

articles = {}
existing_command_map = {}

with open(story_file_path, 'r') as f:

    init_reading = True
    article = Article()
    in_window_enabled = False
    last_line_was_empty = False

    for line in f.readlines():

        trimmed_line = line.strip()

        if trimmed_line.startswith('**') and trimmed_line.endswith('**'):
            init_reading = False

            if len(article.name) > 0 and (len(article.text_lines) > 0 or len(article.text_lines_in_window) > 0 or len(article.alias_of) > 0):

                articles[article.name] = article

            article = Article()
            article.name = trimmed_line.replace('**', '').split(' ')[0].replace('`', '')
            if article.name == 'hra':
                article.command = 'hra'
            if article.name == '1':
                article.name = 'start'
                article.command = 'start'
            if article.name == '2':
                article.name = '7931'
                article.command = '7931'
            if article.name == '5A':
                article.name = '1989'
                article.command = '1989'
            search_val = article.name.replace('-hint', '')
            if article.name.endswith('-hint'):
                for key,val in existing_command_map.items():
                    if val == search_val:
                        article.command = key + '-hint'
                        break
            while article.command == '' or article.command in existing_command_map.keys():
                article.command = str(random.randrange(100, 999))
                if article.name.endswith('hint'):
                    article.command = article.command + '-hint'
            existing_command_map[article.command] = article.name
            article.encrypted = 'zašifrovaný' in trimmed_line
            if article.encrypted:
                article.password = trimmed_line.split('heslem:')[1].replace(')**', '').strip();
            continue

        if init_reading:
            continue

        if trimmed_line.startswith('###') or trimmed_line.startswith('>'):
            continue

        if line.startswith(' ') or line.startswith('	'):
            continue

        if trimmed_line.startswith('::'):
            in_window_enabled = not in_window_enabled
            continue

        if trimmed_line.startswith('=='):
            article.alias_of = trimmed_line.replace('==', '')
            continue

        line_is_empty = len(trimmed_line) == 0

        if last_line_was_empty and line_is_empty:
            continue

        if in_window_enabled:
            article.text_lines_in_window.append(line)
        else:
            article.text_lines.append(line)

        last_line_was_empty = line_is_empty

bashrc_aliases = []

def remove_first_empty_line(lines):
    for line in lines:
        if len(line.strip()) == 0:
            popped = lines.pop(0)
        break

for key,value in articles.items():

    article = Article()
    if len(value.alias_of) > 0:
        article_ref = articles.get(value.alias_of)
        article.name = value.name
        article.command = value.command
        article.text_lines = article_ref.text_lines
        article.text_lines_in_window = article_ref.text_lines_in_window
        article.alias_of = value.alias_of
        article.encrypted = article_ref.encrypted
        article.password = article_ref.password
    else:
        article = value

    # remove empty lines from start
    remove_first_empty_line(article.text_lines)
    remove_first_empty_line(article.text_lines_in_window)

    # replace names with commands
    new_text_lines = []
    for line in article.text_lines:
        new_line = line
        for cmd,name in existing_command_map.items():
            if new_line.startswith(f'{name} - '):
                new_line = new_line.replace(f'{name} - ', f'{cmd} - ')
        new_text_lines.append(new_line)
    article.text_lines = new_text_lines

    alias_name = article.command

    alias_line = ''

    file_base_path = articles_path + '/' + article.command

    if len(article.text_lines_in_window) > 0:
        with open(file_base_path + '_in_window.txt', 'w') as f:
            f.writelines(article.text_lines_in_window)

    std_file_path = f'{file_base_path}.txt'
    with open(std_file_path, 'w') as f:
        f.writelines(article.text_lines)

    # puzzle encryption
    if article.encrypted:
        os.system(f'echo "{article.password}" | gpg -c --no-symkey-cache --batch --yes --passphrase-fd 0 "{std_file_path}"')
        os.remove(std_file_path)

    alias_line = f'alias {alias_name}="$HOME/.project/talos-svatba-web/OS-setup/.bin/article_open.sh \\"{article.command}\\""\n'

    bashrc_aliases.append(alias_line)

with open(articles_path + '/.bashrc_aliases', 'w') as f:
    f.writelines(bashrc_aliases)

print('Script finished')
