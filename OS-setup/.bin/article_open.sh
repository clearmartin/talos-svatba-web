#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

name=$1
dir_path="articles"
file_path_regular="${dir_path}/${name}.txt"
file_path_regular_encrypted="${dir_path}/${name}.txt.gpg"
file_path_window="${dir_path}/${name}_in_window.txt"

if [[ -f "${file_path_window}" ]]; then
    .bin/open_in_subwindow.py ${file_path_window}
fi

if [[ -f "${file_path_regular}" ]]; then
    cat ${file_path_regular}
    exit 0
fi

if [[ -f "${file_path_regular_encrypted}" ]]; then
    echo "Dešifruji..."
    echo ""
    gpg -d -q --no-symkey-cache ${file_path_regular_encrypted}

    rc=$?
    if [[ $rc != 0 ]] ; then
        echo ''
        echo '******************'
        echo 'Nesprávná odpověď.'
        echo '******************'
        echo ''
        exit $rc
    fi
    exit 0
fi

echo "Nepodařilo se najít požadovaný soubor."
exit 1
