#!/usr/bin/env python3

import curses, traceback, textwrap, sys, time
from os.path import exists

if len(sys.argv) != 2:
    print('Provide path to file as parameter.')
    exit(1)

filename = sys.argv[1]

if not exists(filename):
    print('File \'' + filename + '\' does not exist.')
    exit(1)

text_lines = []
with open(filename, 'r') as f:
    text_lines = f.readlines()

try:
    # must always be called to init curses
    stdscr = curses.initscr()
    curses.start_color()
    curses.noecho()

    # measure current screen
    screen = curses.newwin(0, 0)
    screen_height, screen_width = screen.getmaxyx()

    # prepare window size
    win_width = int(screen_width * 0.75)
    win_height = int(screen_height * 0.75)
    win_x_offset = int((screen_width - win_width) / 2)
    win_y_offset = int((screen_height - win_height) / 2)

    # draw box
    box = screen.subwin(win_height, win_width, win_y_offset, win_x_offset)
    box.box()

    def write_esc(red=False):
        if red:
            curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
            screen.addstr(win_y_offset, win_x_offset + win_width - 10, ' Esc/Q X ', curses.color_pair(1))
            return
        screen.addstr(win_y_offset, win_x_offset + win_width - 10, ' Esc/Q X ')

    write_esc()

    pad_width = win_width - 6 + 1
    pad_height = win_height - 4
    pad_height_page_amount = int(pad_height / 2)
    pad = screen.subpad(pad_height, pad_width, win_y_offset + 2, win_x_offset + 3)

    lines = []
    for line in text_lines:
        new_lines = textwrap.wrap(line, pad_width-1)
        if len(new_lines) > 0:
            lines.extend(new_lines)
        else:
            lines.append('')

    line_count = len(lines)
    pad_pos = 0

    def write_pad(pos):
        pad_end = min(pos + pad_height, line_count)
        pad.clear()
        k = 0
        for i in range(pos, pad_end):
            pad.addstr(k, 0, lines[i])
            k = k + 1
        pad.refresh()

        write_scrollbar(pos, pad_end)

        screen.addstr(0, 0, '')


    def write_scrollbar(pad_start, pad_end):
        if pad_height >= line_count:
            return # no scrollbar
        scrollbar_height = max(2, int((pad_height / line_count) * pad_height))

        position_range = pad_height + 2 - scrollbar_height
        scrollbar_from = 0

        if pad_start == 0:
            scrollbar_from = 0
        elif pad_start == (line_count - pad_height):
            scrollbar_from = position_range
        else:
            scrollbar_from = int(pad_start / (line_count - pad_height) * position_range)
            if scrollbar_from == 0:
                scrollbar_from = 1
            if scrollbar_from == position_range:
                scrollbar_from = position_range - 1

        for i in range(0, win_height - 2):
            screen.addstr(win_y_offset + 1 + i, win_x_offset + win_width, ' ')

        for i in range(0, scrollbar_height):
            screen.addstr(win_y_offset + 1 + scrollbar_from + i, win_x_offset + win_width, '█')



    write_pad(pad_pos)


    c = screen.getch() # Get char  and c != 27
    esc = False
    while c != ord('q') and c != ord('Q') and c != ord('x') and c != ord('X'): # Exit loop if char caught is 'q' or ESC
        if str(c) == '27':
            if esc:
                break;
            else:
                esc = True
        else:
            esc = False

        write_esc(esc)

        if c == 65: # ARROW_UP
            pad_pos = max(pad_pos - 1, 0)
            write_pad(pad_pos)
        elif c == 66: # ARROW_DOWN
            pad_pos = min(pad_pos + 1, max(0, line_count - pad_height))
            write_pad(pad_pos)
        elif c == 53 or c == 98: # PAGE_UP / b
            pad_pos = max(pad_pos - pad_height_page_amount, 0)
            write_pad(pad_pos)
        elif c == 54 or c == 10 or c == 32 or c == 102: # PAGE_DOWN / enter / space / f
            pad_pos = min(pad_pos + pad_height_page_amount, max(0, line_count - pad_height))
            write_pad(pad_pos)
        else:
            screen.addstr(0, 0, '')

        # print pressed keycode
        #screen.addstr(0, 0, '    ')
        #screen.addstr(0, 0, str(c))
        #time.sleep(0.5)

        c = screen.getch()

finally:
    curses.endwin()
