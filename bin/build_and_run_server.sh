#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..
BASE=`pwd`

cd $BASE/rust_server
cargo build --release
rc=$?
if [[ $rc != 0 ]] ; then
    echo 'Build failed.'
    exit $rc
fi

cd $BASE/angular
npm run build
rc=$?
if [[ $rc != 0 ]] ; then
    echo 'Build failed.'
    exit $rc
fi

mv $BASE/angular/dist/svatba_final $BASE/angular/dist/svatba_final.trash
mv $BASE/angular/dist/svatba $BASE/angular/dist/svatba_final
rm -rf $BASE/angular/dist/svatba_final.trash

cd $BASE/rust_server
./start_nohup.sh

echo "build script finished"
