CREATE DATABASE talosSvatba DEFAULT CHARACTER SET utf8;
GRANT SELECT ON talosSvatba.* to 'talosSvatba_read'@'localhost' IDENTIFIED BY 'talosSvatba';
GRANT SELECT ON talosSvatba.* to 'talosSvatba_write'@'localhost' IDENTIFIED BY 'talosSvatba';
GRANT INSERT ON talosSvatba.* to 'talosSvatba_write'@'localhost' IDENTIFIED BY 'talosSvatba';
GRANT UPDATE ON talosSvatba.* to 'talosSvatba_write'@'localhost' IDENTIFIED BY 'talosSvatba';
GRANT DELETE ON talosSvatba.* to 'talosSvatba_write'@'localhost' IDENTIFIED BY 'talosSvatba';

--DROP DATABASE talosSvatba;
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'talosSvatba_read'@'localhost';
--REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'talosSvatba_write'@'localhost';
--DROP USER 'talosSvatba_read'@'localhost';
--DROP USER 'talosSvatba_write'@'localhost';
