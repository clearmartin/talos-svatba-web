#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

./stop_server.sh

rm nohup.out

nohup cargo run --release &

echo "Server started."
