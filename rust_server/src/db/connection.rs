use sqlx::{MySqlPool};
use sqlx::mysql::MySqlPoolOptions;

// change for different database
pub type DbPool = MySqlPool;

pub async fn create_pool(count: u32) -> Result<DbPool, sqlx::Error> {

    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");

    MySqlPoolOptions::new()
        .max_connections(count)
        .connect(&database_url).await
}