use serde::{Deserialize, Serialize};
use sqlx::{FromRow};
use chrono::{DateTime, Utc};
use rand::{distributions::Alphanumeric, Rng};

use super::connection;
use connection::DbPool;

#[derive(Serialize)]
pub struct Login {
    pub id: i32,
    pub login_code: String,
    pub name: String,
    pub email: String,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
}

#[derive(Serialize, Deserialize)]
pub struct LoginDto {
    pub login_code: String,
    pub name: String,
    pub email: String,
}

#[derive(Serialize, FromRow)]
#[serde(rename_all = "camelCase")]
pub struct Form {
    pub id: i32,
    pub name: String,
    pub type_name: String,
    pub description: Option<String>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
}

#[derive(Serialize, FromRow)]
#[serde(rename_all = "camelCase")]
pub struct FormWithFields {
    pub id: i32,
    pub name: String,
    pub type_name: String,
    pub description: Option<String>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub fields: Vec<FormField>,
}

#[derive(Serialize, FromRow)]
#[serde(rename_all = "camelCase")]
pub struct FormField {
    pub name: String,
    pub type_name: String,
    pub value: String,
}

impl Login {

    pub async fn find_by_login_code(db_pool: &DbPool, login_code: &String) -> std::io::Result<Option<Login>> {
        let row: Option<Login> = sqlx::query_as!(Login,
            r#"
                SELECT id, login_code, name, email, created, modified
                FROM Person
                WHERE login_code = ?
                LIMIT 1
            "#,
            login_code
        ).fetch_optional(db_pool).await.unwrap();
        Ok(row)
    }

    pub async fn find_privileged_by_login_code(db_pool: &DbPool, login_code: &String) -> std::io::Result<Option<Login>> {
        let row: Option<Login> = sqlx::query_as!(Login,
            r#"
                SELECT id, login_code, name, email, created, modified
                FROM Person
                WHERE login_code = ? AND privileged = 1
                LIMIT 1
            "#,
            login_code
        ).fetch_optional(db_pool).await.unwrap();
        Ok(row)
    }

    pub async fn create_one(db_pool: &DbPool, name: &String, email: &String) -> std::io::Result<Login> {

        let claim_code_generated: String = rand::thread_rng().sample_iter(&Alphanumeric).take(50).map(char::from).collect();

        sqlx::query!(
                r#"
                    INSERT Person (login_code, name, email)
                    VALUES (?, ?, ?)
                "#,
                claim_code_generated, name, email
            ).execute(db_pool).await.unwrap().last_insert_id();
        let res = Login::find_by_login_code(db_pool, &claim_code_generated).await?.unwrap();
        Ok(res)
    }

    pub async fn update_one(db_pool: &DbPool, id: i32, name: &String, email: &String) -> std::io::Result<i32> {
        sqlx::query!(
                r#"
                    UPDATE Person SET name = ?, email = ?
                    WHERE id = ?
                "#,
                name, email, id
            ).execute(db_pool).await.unwrap();
        Ok(id)
    }

    pub async fn update_or_create(db_pool: &DbPool, login_code: &Option<String>, name: &String, email: &String) -> std::io::Result<Login> {
        match login_code {
            Some(code) => {
                let res = Login::find_by_login_code(db_pool, &code).await?;
                match res {
                    Some(db_login) => {
                        Login::update_one(db_pool, db_login.id, name, email).await?;
                        Ok(db_login)
                    },
                    None => {
                        let created = Login::create_one(db_pool, name, email).await?;
                        Ok(created)
                    }
                }
            }
            None => {
                let created = Login::create_one(db_pool, name, email).await?;
                Ok(created)
            }
        }
    }

}

impl Form {

    pub async fn find_by_name_and_type(db_pool: &DbPool, name: &String, type_name: &String) -> std::io::Result<Vec<Form>> {
        let recs: Vec<Form> = sqlx::query_as!(Form,
            r#"
                SELECT id, name, type_name, description, created, modified
                FROM Form
                WHERE name = ? AND type_name = ? AND last_version = 1
                ORDER BY id
            "#,
            name, type_name
        ).fetch_all(db_pool).await.unwrap();
        Ok(recs)
    }

    pub async fn find_by_name_and_type_and_creator(db_pool: &DbPool, name: &String, type_name: &String, creator_id: i32) -> std::io::Result<Option<Form>> {
        let form: Option<Form> = sqlx::query_as!(Form,
            r#"
                SELECT id, name, type_name, description, created, modified
                FROM Form
                WHERE name = ? AND type_name = ? AND last_version = 1 AND creator_id = ?
                ORDER BY id DESC
            "#,
            name, type_name, creator_id
        ).fetch_optional(db_pool).await.unwrap();
        Ok(form)
    }


    pub async fn create_one(db_pool: &DbPool, name: &String, type_name: &String, creator_id: i32) -> std::io::Result<i32> {
        let result_id = sqlx::query!(
                r#"
                    INSERT Form (name, type_name, creator_id, version_id)
                    VALUES (?, ?, ?, 1)
                "#,
                name, type_name, creator_id
            ).execute(db_pool).await.unwrap().last_insert_id();

        sqlx::query!(
                r#"
                    UPDATE Form SET last_version = 0
                    WHERE name = ? AND type_name = ? AND creator_id = ? AND version_id = 1 AND id != ?
                "#,
                name, type_name, creator_id, result_id
            ).execute(db_pool).await.unwrap();

        Ok(result_id as i32)
    }

}

impl FormField {

    pub async fn find_by_form(db_pool: &DbPool, form_id: i32) -> std::io::Result<Vec<FormField>> {
        let recs: Vec<FormField> = sqlx::query_as!(FormField,
            r#"
                SELECT name, type_name, value
                FROM FormField
                WHERE form_id = ?
                ORDER BY name
            "#,
            form_id
        ).fetch_all(db_pool).await.unwrap();
        Ok(recs)
    }

    pub async fn create_one(db_pool: &DbPool, form_id: i32, name: &String, type_name: &String, value: &String) -> std::io::Result<i32> {
        let result_id = sqlx::query!(
                r#"
                    INSERT FormField (form_id, name, type_name, value)
                    VALUES (?, ?, ?, ?)
                "#,
                form_id, name, type_name, value
            ).execute(db_pool).await.unwrap().last_insert_id();
        Ok(result_id as i32)
    }

}
