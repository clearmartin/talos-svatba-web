use actix_web::{get, App, HttpResponse, HttpServer, Responder, cookie::SameSite, middleware::Logger, web};
use actix_session::SessionMiddleware;
use std::env;
use actix_session::config::CookieContentSecurity;
use actix_session::storage::CookieSessionStore;
use actix_web::cookie::Key;
use env_logger::Env;
use rand::Rng;

mod db;
mod app;
mod route;


#[get("/")]
async fn welcome() -> impl Responder {
    HttpResponse::Ok().body("Talos works!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    let db_pool = db::create_pool(5).await.unwrap();

    let host = env::var("BIND_HOST").expect("BIND_HOST is not set in .env file");
    let port = env::var("BIND_PORT").expect("BIND_PORT is not set in .env file");
    let forms_enabled = env::var("FORMS_ENABLED").expect("FORMS_ENABLED is not set in .env file");

    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();

    let private_key = rand::thread_rng().gen::<[u8; 32]>();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            //.wrap(DefaultHeaders::new().header("Cache-Control", "no-store"))
            //.wrap(CookieSession::private(&private_key).lazy(true).same_site(SameSite::Strict))// <- create cookie based session middleware
            .wrap(
                SessionMiddleware::builder(CookieSessionStore::default(), Key::derive_from( &private_key))
                    .cookie_content_security(CookieContentSecurity::Private)
                    .cookie_same_site(SameSite::Strict)
                    .build() // <- create cookie based session middleware
            )
            .app_data(web::Data::new(app::AppData {
                pool: db_pool.clone(),
                forms_enabled: match forms_enabled.as_str() {
                    "true" => true,
                    _ => false
                }
            }))// pass database pool to application so we can access it inside handlers
            .service(welcome)
            .configure(route::form_init)
            .configure(route::login_init)
    });
    server.bind(format!("{}:{}", host, port))?
        .run()
        .await
}
