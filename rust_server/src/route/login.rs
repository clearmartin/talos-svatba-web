use crate::app::AppData;
use actix_web::{post, web, HttpResponse, Responder};
use serde::{Deserialize};
use actix_web::middleware::DefaultHeaders;
use crate::db::{Login};

#[derive(Deserialize)]
struct LoginQuery {
    login_code: String,
}

#[post("/login")]
async fn login(data: web::Data<AppData>, query: web::Query<LoginQuery>) -> impl Responder {

    if !data.forms_enabled {

        HttpResponse::Gone().body("forms_disabled")

    } else {

        log::debug!("logging in");

        let res = Login::find_by_login_code(&data.get_ref().pool, &query.login_code).await.unwrap();
        match res {
            Some(login) => HttpResponse::Ok().json(login),
            None => HttpResponse::Unauthorized().body("code_not_found")
        }
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/user")
        .wrap(DefaultHeaders::new().add(("Cache-Control", "no-store")))
        .service(login)
    );
}