use crate::app::AppData;
use actix_web::{get, post, web, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use actix_web::middleware::DefaultHeaders;
use crate::db::{Form, FormField, FormWithFields, Login, DbPool};
use chrono::Local;
use std::cmp::Ordering;
use xlsxwriter::prelude::*;
use std::fs;
use uuid::Uuid;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct FormData {
    name: String,
    type_name: String,
    fields: Vec<FormFieldData>,
    login: FormLogin,
}

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct FormLogin {
    login_code: Option<String>,
    name: String,
    email: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct FormFieldData {
    name: String,
    type_name: String,
    value: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct FormQuery {
    name: String,
    type_name: String,
    login_code: String,
    format: Option<ResponseFormat>,
    ordered_field_names: Option<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
enum ResponseFormat {
    Csv,
    Json,
    Xlsx,
}

async fn is_privileged(db_pool: &DbPool, login_code: &String) -> bool {
    return Login::find_privileged_by_login_code(db_pool, login_code).await.unwrap().is_some();
}

#[post("/save")]
async fn save(data: web::Data<AppData>, query: web::Json<FormData>) -> impl Responder {

    if !data.forms_enabled {

        HttpResponse::Gone().body("forms_disabled")

    } else {

        log::debug!("form_save");

        let login = Login::update_or_create(&data.get_ref().pool, &query.login.login_code, &query.login.name, &query.login.email).await.unwrap();

        let result = Form::create_one(&data.get_ref().pool, &query.name, &query.type_name, login.id).await;
        match result {
            Ok(form_id) => {
                for field in &query.fields {
                    FormField::create_one(&data.get_ref().pool, form_id, &field.name, &field.type_name, &field.value).await.unwrap();
                }
                HttpResponse::Ok().json(FormLogin {
                    login_code: Some(login.login_code),
                    name: login.name,
                    email: login.email,
                })
            },
            _ => HttpResponse::BadRequest().body("db_error"),
        }
    }
}

#[get("/list")]
async fn list(data: web::Data<AppData>, query: web::Query<FormQuery>) -> impl Responder {

    log::debug!("get_forms");

    if !is_privileged(&data.get_ref().pool, &query.login_code).await {
        return HttpResponse::Unauthorized().body("login_code_not_found");
    }

    let mut result_items: Vec<FormWithFields> = Vec::new();

    let result = Form::find_by_name_and_type(&data.get_ref().pool, &query.name, &query.type_name).await;
    match result {
        Ok(form_items) => {

            let mut field_names: Vec<String> = vec![];

            for form in form_items {
                let fields = FormField::find_by_form(&data.get_ref().pool, form.id).await.unwrap();

                for field in &fields {
                    let field_name = &field.name;
                    if !field_names.contains(field_name) {
                        field_names.push(field_name.clone());
                    }
                }

                result_items.push(FormWithFields {
                    id: form.id,
                    name: form.name,
                    type_name: form.type_name,
                    description: form.description,
                    created: form.created,
                    modified: form.modified,
                    fields,
                });

            }

            // order fields if requested
            if query.ordered_field_names.is_some() {
                let ordered_field_names = query.ordered_field_names.as_ref().unwrap().split(",").collect::<Vec<&str>>();
                field_names.sort_by(|a, b| {
                    let a_position = ordered_field_names.iter().position(|x| x.eq(a));
                    let b_position = ordered_field_names.iter().position(|x| x.eq(b));
                    if a_position.is_some() && b_position.is_some() {
                        let res = a_position.unwrap() as i32 - b_position.unwrap() as i32;
                        return if res < 0 {
                            Ordering::Less
                        } else if res > 0 {
                            Ordering::Greater
                        } else {
                            Ordering::Equal
                        }
                    }
                    b.cmp(a)
                });
            }

            match &query.format {
                Some(ResponseFormat::Csv) => {

                    let mut writer = csv::Writer::from_writer(vec![]);

                    let mut header_written = false;

                    for item in result_items {

                        if !header_written {
                            let mut row: Vec<String> = Vec::new();
                            row.push(String::from("id"));
                            row.push(String::from("created"));
                            row.push(String::from("modified"));

                            for field_name in &field_names {
                                row.push(field_name.clone());
                            }

                            writer.write_record(row).unwrap();
                            header_written = true;
                        }

                        let mut row: Vec<String> = Vec::new();
                        row.push(item.id.to_string());
                        row.push(item.created.to_rfc3339());
                        row.push(item.modified.to_rfc3339());

                        for field_name in &field_names {
                            let found_field_res = item.fields.binary_search_by(|probe| probe.name.cmp(field_name));
                            match found_field_res {
                                Ok(idx) => {
                                    let found_field = item.fields.get(idx).unwrap();
                                    row.push(found_field.value.clone());
                                }
                                _ => {
                                    row.push(String::from(""));
                                }
                            }
                        }

                        writer.write_record(row).unwrap();
                    }

                    writer.flush().unwrap();

                    HttpResponse::Ok()
                        .content_type("text/csv")
                        .append_header(("Content-Disposition", format!("attachment; filename=\"export_{}_{}.csv\"", &query.name, Local::now().format("%Y-%m-%dT%H-%M-%S"))))
                        .body(String::from_utf8(writer.into_inner().unwrap()).unwrap())
                }
                Some(ResponseFormat::Xlsx) => {

                    let temp_filename = format!("{}.xlsx", Uuid::new_v4());
                    let workbook = Workbook::new(temp_filename.as_str()).unwrap();

                    let mut sheet1 = workbook.add_worksheet(None).unwrap();

                    sheet1.write_string(0, 0, "id", None).unwrap();
                    sheet1.write_string(0, 1, "created", None).unwrap();
                    sheet1.write_string(0, 2, "modified", None).unwrap();
                    let mut k = 3;
                    for field_name in &field_names {
                        sheet1.write_string(0, k, field_name.clone().as_str(), None).unwrap();
                        k = k + 1;
                    }
                    let last_col = k - 1;

                    sheet1.set_column(1, last_col, 30.0, None).unwrap();

                    let mut row_num = 1;
                    for item in result_items {

                        sheet1.write_string(row_num, 0, item.id.to_string().as_str(), None).unwrap();
                        sheet1.write_string(row_num, 1, item.created.to_rfc3339().as_str(), None).unwrap();
                        sheet1.write_string(row_num, 2, item.modified.to_rfc3339().as_str(), None).unwrap();

                        k = 3;
                        for field_name in &field_names {
                            let found_field_res = item.fields.binary_search_by(|probe| probe.name.cmp(field_name));
                            match found_field_res {
                                Ok(idx) => {
                                    let found_field = item.fields.get(idx).unwrap();
                                    sheet1.write_string(row_num, k, found_field.value.as_str(), None).unwrap();
                                }
                                _ => {
                                    sheet1.write_string(row_num, k, "", None).unwrap();
                                }
                            }
                            k = k + 1;
                        }

                        row_num = row_num + 1;
                    }

                    sheet1.autofilter(0, 0, row_num - 1, last_col).unwrap();

                    workbook.close().unwrap();
                    let result_bytes = fs::read(temp_filename.clone()).expect("cannot read file");
                    fs::remove_file(temp_filename).expect("cannot delete file");

                    HttpResponse::Ok()
                        .content_type("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        .append_header(("Content-Disposition", format!("attachment; filename=\"export_{}_{}.xlsx\"", &query.name, Local::now().format("%Y-%m-%dT%H-%M-%S"))))
                        .body(result_bytes)
                }
                _ => HttpResponse::Ok().json(result_items)
            }
        },
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}

#[get("/load")]
async fn load(data: web::Data<AppData>, query: web::Query<FormQuery>) -> impl Responder {

    log::debug!("get_forms");

    let login = Login::find_by_login_code(&data.get_ref().pool, &query.login_code).await.unwrap();
    if login.is_none() {
        return HttpResponse::Unauthorized().body("login_code_not_found");
    }

    let result = Form::find_by_name_and_type_and_creator(&data.get_ref().pool, &query.name, &query.type_name, login.unwrap().id).await;
    match result {
        Ok(form_option) => {
            match form_option {
                Some(form) => {
                    let fields = FormField::find_by_form(&data.get_ref().pool, form.id).await;

                    let prepared_form = FormWithFields {
                        id: form.id,
                        name: form.name,
                        type_name: form.type_name,
                        description: form.description,
                        created: form.created,
                        modified: form.modified,
                        fields: fields.unwrap(),
                    };

                    HttpResponse::Ok().json(prepared_form)
                }
                None => HttpResponse::NotFound().body("not_yet_stored")
            }
        },
        _ => HttpResponse::BadRequest().body("db_error"),
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/form")
        .wrap(DefaultHeaders::new().add(("Cache-Control", "no-store")))
        .service(save)
        .service(list)
        .service(load)
    );
}