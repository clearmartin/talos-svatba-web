use crate::db::DbPool;

pub struct AppData {
    pub pool: DbPool,
    pub forms_enabled: bool,
}