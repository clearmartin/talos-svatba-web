# HRA - ZÁKLADNÍ INFORMACE, PŘÍBĚH, OBSAH, APOD.

V tomto souboru budou základní informace k počítačové hře, kterou budeme mít na počítačích přímo na svatbě.

DŮLEŽITÉ: soubor mindmap.xmind obsahuje mapu příběhu. Je to výrazně příjemnější na prohlížení než ten texťák :))
Akorát je k tomu potřeba program Xmind (https://xmind.app).

### TODO list
- Zajistit minimálně jednu, možná dvě PS/2 klávesnice
        - Zatím mám jednu přihozenou na Aukru, uvidíme jestli vyjde ji mít levněji než za 120 korun...
Když tak tam jsou ale i nějaké další.
- Martinův taťka by je měl sehnat.
- Budu potřebovat ještě jednu redukci - zatím mám plán ji normálně koupit na Alze a pak ji do 14 dnů vrátit.
Utrácet za tuhle blbost víc než jednou je fakt zbytečný.

---

### Obecně:
- Budeme mít 2 paralelní příběhy -> Unravelíka a Unravelinku.
- Jejich příběhy se budou různě spojovat a rozdělovat - ve smyslu Unravel, Degrees of Separation, Talos Principle a podobně.
- Budeme mít 2 počítače - to znamená, že můžeme dávat např. nápovědy tím způsobem, že u Unravelíka bude polovina a u
Unravelinky druhá polovina té nápovědy. Ať už obrázek, text, cokoli... A ideálně by to člověk měl mít za úkol kombinovat
v reálném čase.
- To ale nesmí být hlavní náplň, protože to bychom tu hru vlastně zablokovali na jenom jednu session, kdežto jinak
bychom mohli mít dvě session díky dvěma počítačům.

---

### Herní model
- Celá ta hra by měla být zhruba na 1-2 hodiny... Aby si ji jednak mohlo vyzkoušet víc lidí po sobě, a aby to lidi bavilo a
měli šanci neztratit zájem, zároveň dojít do konce a zároveň se bavit i na párty, a nestrávit celou dobu jenom u počítače.
- Můžeme to zkombinovat s tím, že u počítače lidi průměrně stráví třeba hodinu, ale u jiných činností mohou strávit i více
času. To by možná bylo taky zajímavé. Ale zase bych to nepřeháněl, není to to hlavní, hlavní je párty a tohle je bonusík navíc. :)
- Model hry by se mi nejvíce líbil takovýhle:
	1. Dialog, který dá nějaké množství možností/rozhodnutí na výběr -> 1-4 jednotlivé příkazy do terminálu
	2. Tyhle dialogy vyústí v nějaký úkol nebo puzzle -> může být fyzický nebo digitální
	3. Vyřešení úkolu/puzzlu poskytne entrypoint do další dialogové sekvence
	4. Toto se zopakuje několikrát a pokaždé se těmi dialogy posune příběh. Bonusové body za to, pokud i samotný úkol dá něco do příběhu.
- Zároveň tyto modely budou dva, jeden pro Unravelíka a jeden pro Unravelinku, jak jsem už psal. To znamená, že můžeme docela
dobře vytvořit zvládnutelné, relativně krátké dva příběhy, které obsahují nějaké pro nás zásadní hodnoty/věci, ale přitom
jsou dostatečně košaté - jde o to, že v tomhle modelu by odhadem stačilo celkem třeba 10-15 úkolů na vícero linií v každém z příběhů.

---
