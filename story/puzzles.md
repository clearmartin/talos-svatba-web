# VŠECHNY PUZZLY, KTERÉ DÁME DO HRY

**Knihovna**
Nápis/symbol na knihovně, ukazující na nějakou knížku a pasáž v ní.
Můžeme jich přirozeně mít i víc, že ano. :)

**Lockpicking**
Nějaký lockbox s jednoduchým visacím zámkem, pár lockpicky u toho a případně jednoduchý návod, jak s tím pracovat.
V lockboxu bude herní věc, tj. třeba název dalšího příkazu, kterým pokračovat.
Bude to asi na baru, abychom nad tím měli trochu kontrolu a hlavně to mohli vždycky zamknout a dát to dalšímu člověku.

**Hádanky**
Hádanky! Pár bych jich dal, podle mě je to docela cheap, ale v pohodě a acceptable.

- Co se musí rozbít, než se to použije?
Odpověď: vejce

- Co je tak křehké, že samotné vyslovení jména jej prolomí?
Odpověď: ticho

- Co vyhodím, když to chci použít, ale znovu vezmu, když už to používat nechci?
Odpověď: kotva

- Co je vždycky před námi, ale přitom to nemůžeme vidět?
Odpověď: budoucnost

- Co můžeš zlomit, i když se toho nedotkneš?
Odpověď: slib

- Hory padnou, zámky se zbortí, cesty zmizí... A nakonec zůstane jen jediné.
Odpověď: čas

**Hledání v obrázku**
Obrázek, ve kterém budeš muset najít nějaký detail - real life pixel hunting!

**GPG šifrované soubory**
Soubory zašifrované heslem, které bude někde k nalezení. Obecně by tohle mohla být
mechanika, kde odpověď na nějaký fyzický úkol bude heslo k souboru v počítači.
Dešifrování souboru poskytne nějaký dokument, který zase dá nějaké backstory a 
zároveň řekne, co máš napsat, aby ses mohl posunout dál v příběhu Unravelíků 
(jinými slovy alias, které začíná další sekvenci dialogů).
