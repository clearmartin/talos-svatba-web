# Překlad Questioning doubt conf.txt

Original:

Keynote Speech by N. Sarabhai, "Questioning Doubt"

They say "doubt everything," but I disagree. Doubt is useful in small amounts, but too much of it leads to apathy and confusion. No, don't doubt everything. QUESTION everything. That's the real trick. Doubt is just
 a lack of certainty. If you doubt everything, you'll doubt evolution, science, faith, morality, even reality itself - and you'll end up with nothing, because doubt doesn't give anything back. But questions 
have answers, you see. If you question everything, you'll find that a lot of what we believe is untrue… but you might also discover that some things ARE true. You might discover what your own beliefs 
are. And then you'll question them again, and again, eliminating flaws, discovering lies, until you get as close to the truth as you can.

Questioning is a lifelong process. That's precisely what makes it so unlike doubt. Questioning engages with reality, interrogating all it sees. Questioning leads to a constant assault on the intellectual status
 quo, where doubt is far more likely to lead to resigned acceptance. After all, when the possibility of truth is doubtful (excuse the pun), why not simply play along with the most convenient lie?e #%&%§/$ 

Questioning is progress, but doubt is stagnation. 

***

Překlad:

Úryvek z projevu N. Sarabhai, "Zpochybnění pochybnosti"

Říká se, že máme "pochybovat o všem", s tím však nesouhlasím. Pochybnost je užitečná v malém množství, ale jakmile je jí moc, vede k apatii a zmatení. Ne, nepochybujte o všem. PTEJTE SE na všechno. To je 
ten trik. Pochybnost je jenom nedostatek jistoty. Pokud byste pochybovali o všem, tak začnete zpochybňovat evoluci, vědu, víru, morálku, a dokonce samotnou realitu - a v rukou vám nezůstane nic, protože 
pochybnost nic nepřináší. Ale otázky mají odpovědi, chápete. Když se budete na všechno ptát, tak zjistíte, že spousta z toho co se učíme o světě není pravdivá... Ale taky možná objevíte, že některé věci 
pravdivé JSOU. Možná díky tomu najdete svá vlastní přesvědčení. A na ta se budete znovu a znovu ptát, zatímco budete eliminovat nedostatky a odhalovat lži, dokud se nedostanete tak blízko k pravdě, 
jak je to jen možné.

Otázky jsou celoživotní proces. A to je přesně to, co je odlišuje od pochybností. Ptaní se dotýká reality a vztahuje k ní všechno co vidí. Ptaní se vede k nekončícímu napadání intelektuálního statutu quo, 
zatímco pochybnost daleko spíš povede k rezignovanému přijetí. Koneckonců, pokud samotná možnost existence pravdy je pochybná, tak proč prostě nezůstat u některé z pohodlných lží? /7931/

Otázky jsou vývoj, pochybnosti jsou stagnace.
