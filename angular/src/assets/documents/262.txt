"To až zase ztratíš klíče a zapomeneš, kam sis schoval náhradní.

Takže od začátku:

Obejdi dům.
A najdi malou krabičku na parapetu.

Budeš muset ten zámek otevřít paklíčem, ale svalová paměť naštěstí nezapomíná.
Paklíče jsou na spodní straně krabičky.

Akorát... počkat, tohle neni správně.
Ona občas mizí a objevuje se dost náhodně.
Ta krabička je zrovna v jiným světě.
Konkrétně ve světě toho někoho, co ti tamhle kouká přes rameno.

No, tenhle někdo asi bude muset tu krabičku otevřít za tebe, jinak se dál nedostaneš.

Řekl bych, ať se jde zeptat na bar, ale to už bych radil skoro moc.

Tvoje moudřejší já"
