
Chápeme to, ne vždy je čas podrobně pročítat náhodné hemzíky na internetu. To je obecně celkem dobrá psychohygiena, omezovat čas na náhodných internetech. :) Byť zrovna tento hemzík je ve skutečnosti autentický, je ze skutečných Hovorů k sobě od Marca Aurelia.

Na Kačeří jsme měli Juliin výtisk této knihy vložený mezi ostatní podobné knihy do knihovny, a do něj jsme vepsali ty poznámky, o kterých byla řeč. Nebyla sice pod akátem, ale aspoň tam byly poměrně masivní trámy. A velký strom je na dvorku před domem, jestli se to počítá.

Anyway, tudy prosím dále, nenechte se rušit!

---
7931 - vrátit knihu do knihovny a pokračovat dál.