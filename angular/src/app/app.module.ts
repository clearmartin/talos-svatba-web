import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';

import { TerminalModule } from 'primeng/terminal';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';

import { PrimeNGTerminalPatched } from './components/primeng-extensions/terminal';

import { StateService } from './service/state.service';

import { AppComponent }   from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormComponent } from './components/form/form.component';
import { FormFieldComponent } from './components/form/form-field.component';
import { FormViewComponent } from './components/form/form-view.component';
import { TerminalViewerComponent } from './components/terminal/viewer.component';
import { ImageSlideshowComponent } from './components/image-slideshow/image-slideshow.component';
import { BigEyeComponent } from './components/eye/big-eye.component';
import { BiosBootComponent } from './components/bios-boot/bios-boot.component';
import { LoginSectionComponent } from './components/login-section/login-section.component';
import { StaticInfoComponent } from './components/static-info/static-info.component';
import { StaticInfoPcComponent } from './components/static-info-pc/static-info-pc.component';
import { ShowQrPozvanka } from './components/qr-pozvanka/qr-pozvanka.component';
import { WevpTvComponent } from './components/wevp-tv/wevp-tv.component';
import { SongComponent } from './components/song/song.component';
import { DanceComponent } from './components/dance/dance.component';

@NgModule({
  declarations: [

    PrimeNGTerminalPatched,

    AppComponent,
    HomeComponent,
    FormComponent,
    FormFieldComponent,
    FormViewComponent,
    TerminalViewerComponent,
    ImageSlideshowComponent,
    BigEyeComponent,
    BiosBootComponent,
    LoginSectionComponent,
    StaticInfoComponent,
    StaticInfoPcComponent,
    ShowQrPozvanka,
	  WevpTvComponent,
    SongComponent,
    DanceComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    //TerminalModule,
    ScrollPanelModule,
    TableModule,
    QRCodeModule,
  ],
  providers: [
    StateService,
  ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
