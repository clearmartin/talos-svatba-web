import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ScrollPanel } from 'primeng/scrollpanel';

@Component({
    selector: '[app-image-slideshow]',
    templateUrl: './image-slideshow.component.html',
    styleUrls: ['./image-slideshow.component.scss'],
    preserveWhitespaces: true
})
export class ImageSlideshowComponent implements OnInit {

    @Input() imgUrlList: string[] = [];

    urlCurrent: string = '';
    listIndex: number = -1;
    listProgressPercent: number = 0;
    listProgressWidth: string = '0%';

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this.next();
    }

    @HostListener('document:keydown.arrowleft', [])
    prev() {
        this.nextOrPrev(false);
    }

    @HostListener('document:keydown.arrowright', [])
    next() {
        this.nextOrPrev(true);
    }

    nextOrPrev(next: boolean): void {
        if (next) {
            this.listIndex++;
            if (this.listIndex + 1 > this.imgUrlList.length) {
                this.listIndex = 0;
            }
        } else {
            this.listIndex--;
            if (this.listIndex < 0) {
                this.listIndex = this.imgUrlList.length - 1;
            }
        }

        this.urlCurrent = this.imgUrlList[this.listIndex];
        this.listProgressPercent = Math.round((this.listIndex + 1) / this.imgUrlList.length * 100);
        this.listProgressWidth = `${this.listProgressPercent}%`;
    }

}
