import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WevpTvComponent } from './wevp-tv.component';

describe('WevpTvComponent', () => {
 let component: WevpTvComponent;
 let fixture: ComponentFixture<WevpTvComponent>;

 beforeEach(async () => {
 await TestBed.configureTestingModule({
 declarations: [ WevpTvComponent ]
 })
 .compileComponents();

 fixture = TestBed.createComponent(WevpTvComponent);
 component = fixture.componentInstance;
 fixture.detectChanges();
 });

 it('should create', () => {
 expect(component).toBeTruthy();
 });
});