import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, HostListener, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TerminalService } from 'primeng/terminal';
import { PrimeNGTerminalPatched } from '../primeng-extensions/terminal';
import { BigEyeComponent } from '../eye/big-eye.component';

@Component({
    selector: '[app-home]',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [TerminalService],
    preserveWhitespaces: true
})
export class HomeComponent implements OnInit {

    showBoot: boolean = true;
    welcomeMessage: string = '';
    wrapperClass: string = '';
    eyeClassAppearance: string = '';
    bigEyeClassAppearance: string = '';

    // terminal
    maxTerminalHeight: string = '200px'
    terminalHistory: string[] = [];

    // viewer
    viewerContent: string = '';
    viewerImgUrl: string = '';
    viewerImgUrlList: string[] = [];
    viewerQrCodeValue: string = '';

    eyeAnimation01: string[] = ['one', 'one', 'blink', 'one', 'one', 'four', 'four', 'one', 'one', 'five', 'five', 'blink', 'one'];

    @ViewChild('terminal') terminal!: PrimeNGTerminalPatched;
    @ViewChild('bigEye') bigEye!: BigEyeComponent;
    @ViewChild('smallEye') smallEye!: BigEyeComponent;
    @ViewChild('rowHeader') rowHeader!: ElementRef;

    constructor(private terminalService: TerminalService, private elementRef: ElementRef, private http: HttpClient, private changeDetectorRef: ChangeDetectorRef) {
        this.terminalService.commandHandler.subscribe(command => {
            this.handleTerminalCommand(command);
        });
    }

    ngOnInit(): void {}

    ngAfterViewInit(): void {
    }

    bootFinished(instant: boolean = false) {
        if (this.wrapperClass === 'home') {
            return;
        }
        this.showBoot = false;
        if (instant) {
            this.displayHome();
            return;
        }
        this.wrapperClass = 'welcome';
        this.bigEye.animateEye(this.eyeAnimation01, 500, () => {
            this.animateBigEyeShrink(() => {
                this.displayHome();
            });
        }, false);
    }

    animateBigEyeShrink(callback: () => void = () => {}) {
        this.bigEyeClassAppearance = '';
        const timeout = 200;
        setTimeout(() => {
            this.bigEyeClassAppearance = 'smaller-one';
            setTimeout(() => {
                this.bigEyeClassAppearance = 'smaller-two';
                setTimeout(() => {
                    this.bigEyeClassAppearance = 'smaller-three';
                    setTimeout(() => {
                        this.bigEyeClassAppearance = 'smaller-four';
                        setTimeout(() => {
                            this.bigEyeClassAppearance = 'small';
                            callback();
                        }, timeout);
                    }, timeout);
                }, timeout);
            }, timeout);
        }, timeout);
    }

    displayHome() {
        this.wrapperClass = 'home';
        this.eyeClassAppearance = 'small';
        this.smallEye.animateEye(this.eyeAnimation01, 3000);
        setTimeout(() => {
            this.terminal.focusInput();
            this.updateTerminalHeight();
        }, 10);
    }

    @HostListener('document:keydown.escape', [])
    cancelBoot() {
        console.log('cancel boot');
        this.bootFinished(true);
        this.viewerContent = '';
        this.viewerImgUrl = '';
        this.viewerImgUrlList = [];
        this.viewerQrCodeValue = '';
    }

    @HostListener('window:resize')
    updateTerminalHeight() {
        this.maxTerminalHeight = `${window.innerHeight - 20 - this.rowHeader.nativeElement.clientHeight}px`;
    }

    showQrCodeInViewer(qrCodeValue: string): void {
        this.terminal.disable();
        this.viewerQrCodeValue = qrCodeValue;
    }

    showImageSlideshowInViewer(imgUrls: string[]): void {
        this.terminal.disable();
        this.viewerImgUrlList = imgUrls;
    }

    async openDocument(name: string): Promise<string> {

        const self = this;

        return new Promise((resolve, reject) => {

            if (name.endsWith('.jpg') || name.endsWith('.jpeg') || name.endsWith('.png') || name.endsWith('.webp') || name.endsWith('.gif')) {
                const path = `/assets/images/${name}`;
                var img = new Image();
                img.onload = () => {
                    this.terminal.disable();
                    self.viewerImgUrl = path;
                    resolve('Otevírám obrazová data ve vnořeném okně... hotovo.');
                }
                img.onerror = () => {
                    resolve('Požadovaná obrazová data nebyla nalezena nebo jsou poškozená.');
                };
                img.src = path;
                return;
            }

            this.http.get(`/assets/documents/${name}`, { responseType: 'text' }).subscribe(
                (res: string) => {
                    console.log('success get document', res);
                    if (res.startsWith('<!doctype html>')) {
                        resolve('Požadovaný dokument nebyl nalezen, nebo je nečitelný.');
                        return;
                    }
                    this.terminal.disable();
                    this.viewerContent = res;
                    resolve('Otevírám dokument ve vnořeném okně... hotovo.');
                },
                (err: string) => {
                    console.log('error get document', err);
                    resolve('Požadovaný dokument nebyl nalezen nebo je nečitelný.');
                },
            );
        });
    }

    async handleTerminalCommand(command: string) {
        console.log('command:', command);
        if (command.startsWith('open ') || command.startsWith('ukaž ')) {
            const name = command.substring(5).trim();
            const response = await this.openDocument(name);
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, 'pribehy')) {
            const response = await this.openDocument('příběhy.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, 'scena')) {
            const response = await this.openDocument('scena.jpg');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, 'ubytovani')) {
            const response = await this.openDocument('ubytování.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, 'jidlo') || this.commandMatches(command, 'piti')) {
            const response = await this.openDocument('jídlo.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (command === 'program') {
            const response = await this.openDocument('program.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (command === 'figure_it_out') {
            const response = await this.openDocument('figure_it_out.eml');
            this.terminalService.sendResponse(response);
            return;
        }

        if (command === '262') {
            const response = await this.openDocument('262.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Unravelík se při zmínce o tom, že mu někdo kouká přes rameno, znepokojeně otočil. Ale samozřejmě že tam nikdo nebyl,
  ještě aby ano, když neslyšel nikoho přicházet...
  Každopádně se tedy vydal kolem domu, jenom tak pro jistotu... Ale krabička tam skutečně nebyla. Tak že by ten papír
  přece jen mluvil pravdu, a Unravelík teď musel počkat, až ten "někdo" z jiného světa vyřeší tenhle úkol?
  Vrátil se zatím zpět k předním dveřím a sedl si do houpacího křesla.
  Chvilku počká a uvidí.
  Třeba to ten "někdo" vyřeší za něj. To by byla hezká spolupráce, takhle napříč světy. Ať už to má znamenat cokoli.
  Unravelík ještě začal rozvíjet teorie o tom, že se jedná o vícerozměrnou bytost, kterou on sám nemůže vidět, ale
  ona může vidět jej... Ale pak ho příjemné zvuky lesa ukolébaly ke spánku.
  
  ---
  262-hint - Pokud si nevíte rady, napište tento příkaz.

`);
            return;
        }

        if (this.commandMatches(command, '262-reseni')) {
            const response = await this.openDocument('262-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (command === '903') {
            const response = await this.openDocument('903.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Unravelík se při zmínce o tom, že mu někdo kouká přes rameno, znepokojeně otočil. Ale samozřejmě že tam nikdo
  nebyl, ještě aby ano, když neslyšel nikoho přicházet...
  Že by ten papír přece jen mluvil pravdu, a Unravelík teď musel počkat, až ten "někdo" z jiného světa vyřeší
  tenhle úkol?
  Vrátil se zatím zpět k předním dveřím a sedl si do houpacího křesla.
  Chvilku počká a uvidí.
  Třeba to ten "někdo" vyřeší za něj. To by byla hezká spolupráce, takhle napříč světy. Ať to znamená cokoli.
  Unravelík ještě začal rozvíjet teorie o tom, že se jedná o vícerozměrnou bytost, kterou on sám nemůže vidět, ale
  ona může vidět jeho... Ale pak ho příjemné zvuky lesa ukolébaly ke spánku.

  ---
  903-hint - Pokud si nevíte rady, napište tento příkaz.

`);
            return;
        }

        if (this.commandMatches(command, '903-reseni')) {
            const response = await this.openDocument('903-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '936-reseni')) {
            const response = await this.openDocument('936-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '625-reseni')) {
            const response = await this.openDocument('625-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '279-reseni')) {
            const response = await this.openDocument('279-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '344-reseni')) {
            const response = await this.openDocument('344-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, 'hovory')) {
            const response = await this.openDocument('hovory.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (command === '347') {
            const response = await this.openDocument('347.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Unravelík se nervózně rozhlédl - co to má znamenat, že nemá moc času?
  Pak tedy poodstoupil a otočil se, přičemž napůl čekal, že na něj něco vyskočí... Naštěstí nevyskočilo.
  Ale na hranici lesa dole z kopce skutečně viděl obrys nějaké postavy, jak na něj zamávala a pak zmizela do lesa.
  Půjde Unravelík za ní?
  
  ---
  398 - Ano, půjde za neznámou postavou do lesa.
  279 - Ne, raději se vrátí zpět. Koneckonců stejně měl dneska jiné plány, měl by jít do Antických rozvalin.
            
`);
            return;
        }

        if (this.commandMatches(command, '626')) {
            const response = await this.openDocument('626.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Pod nápisem byl zvláštní otvor, který vypadal, jako že je potřeba do něj něco říct.
  Takže... to je hádanka, pomyslel si Unravelík. A já musím říct odpověď na ni do tohohle otvoru. Dobře, tak to
  přece nemůže být tak těžké...
  
  ---
  626-zadání - Znovu zobrazit hádanku.
  626-hint - Pokud chcete nápovědu, napište tento příkaz.
  
  Jak zní odpověď na hádanku?

`);
            return;
        }

        if (this.commandMatches(command, '626-zadani')) {
            const response = await this.openDocument('626.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '626-reseni')) {
            const response = await this.openDocument('626-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '679')) {
            const response = await this.openDocument('679.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Takže další hádanka, skvěle. No, aspoň dokud se nebudu hýbat, tak mám klid na přemýšlení, řekl si Unravelík.
  
  ---
  679-zadání - Znovu zobrazit hádanku.
  679-hint - Pokud chcete nápovědu, napište tento příkaz.

  Jak zní odpověď na hádanku?

`);
            return;
        }

        if (this.commandMatches(command, '679-zadani')) {
            const response = await this.openDocument('679.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '679-reseni')) {
            const response = await this.openDocument('679-reseni.txt');
            this.terminalService.sendResponse(response);
            return;
        }

        if (this.commandMatches(command, '674')) {
            const response = await this.openDocument('674.txt');
            this.terminalService.sendResponse(response);
            this.terminalService.sendResponse(`
  Byl to nějaký dopis... Zvláštní. Byla na něm částečná adresa, která by mu mohla možná pomoci zjistit, o co na
  tomhle místě jde.
  Unravelík pokrčil rameny, schoval si dopis do kapsy - třeba se mu podaří zjistit, kdo je to ta Edith a kam by
  měl být dopis doručen - a pokračoval v cestě.
  
  ---
  519 - Jít dál.

`);
            return;
        }

        const response = this.getResponse(command);
        this.terminalService.sendResponse(response);

    }

    getResponse(command: string): string {
        if (command === 'datum') {
            return new Date().toDateString();
        }

        if (command === 'ahoj') {
            return `
    (・‿・)ノ
             `;
        }

        if (command === 'info') {
    	    return `--------------------------------------------------------------------------------
/                                                                              \\
/                              INFORMACE O SVATBĚ                              \\
/                                                                              \\
/   ふ ČAS: 18.9.2021, obřad začne ve 13:00                                    \\
/       う Pokud chcete být na obřadu, prosíme, abyste dorazili do 12:00       \\
/   ふ MÍSTO: Svatba proběhne v Penzionu Kačeří                                \\
/       う Adresa -> Lachov 14, 547 59 Teplice nad Metují                      \\
/   ふ UBYTOVÁNÍ: Pro informace o ubytování napište 'ubytování'.               \\
/   ふ DOPRAVA:                                                                \\
/       う Autem -> Pro informace o autodopravě napište 'auto'.                \\
/       う Autobusem/vlakem -> Pro zobrazení možných spojů napište 'bus'.      \\
/   ふ PROGRAM: Pro informace o programu napište 'program'.                    \\
/   ふ DRESSCODE: Tématem svatby jsou "Příběhy 20. století".                   \\
/       う Pro inspiraci napište do terminálu 'příběhy'.                       \\
/   ふ DARY: Budeme Vám vděční za jakýkoli finanční příspěvek na naši          \\
/            svatební cestu, lustrů a skleniček máme dostatek. :)              \\
/   ふ JÍDLO: Pro informace o zajištění jídla a pití napište 'jídlo'.          \\
/   ふ FOTKY: napište 'fotky' pro zobrazení snoubenců                          \\
/   ふ DOTAZNÍK: Prosíme Vás, vyplňte nám svatební dotazník! Opravdu.          \\
/       う Napište 'dotazník' - tím budete přesměrováni na formulář.           \\
/                                                                              \\
/   *Pro seznam příkazů napište 'nápověda'. Pro kontakt na nás - 'kontakt'.    \\
--------------------------------------------------------------------------------`;
        }

        if (this.commandMatches(command, 'dotaznik')) {
            this.openHref('/dotaznik');
            return 'Probíhá přesměrování...'
        }

        if (this.commandMatches(command, 'pozvanka')) {
            this.openHref('/qr/pozvanka');
            return 'Probíhá přesměrování...'
        }

        if (command === 'auto') {
          return `Adresa místa: Lachov 14, Teplice nad Metují
`;
        }

        if (command === 'bus') {
          return `Sem přidáme různá doporučená spojení na místo konání svatby,
abychom vám ušetřili trochu práce s hledáním.
`;
        }

        if (command === 'qrtest') {
            // Toto je jen příklad použití qr-kódu
            // Jedná se o verzi 'Will the Circle Be Unbroken' od Freyi Catherine, kterou máme hodně rádi.
            // Je to ta písnička, co jsme měli při svatebním obřadu. :)
            this.showQrCodeInViewer('https://youtu.be/xbpBx2KEsx0');
    	    return `...nalezli jste pokusný QR kód.
`;
        }

        if (command === 'fotky') {
            // příklad použití image-slideshow
            this.showImageSlideshowInViewer([ '/assets/images/my/foto1.jpg',
                                              '/assets/images/my/foto2.jpg',
                                              '/assets/images/my/foto3.jpg',
                                              '/assets/images/my/foto4.jpeg',
                                              '/assets/images/my/foto5.jpg',
                                              '/assets/images/my/foto6.jpg',
                                              '/assets/images/my/foto7.jpg' ]);
    	    return 'Otevírám obrazová data ve vnořeném okně... hotovo.';
        }

        if (command === 'inspirace') {
          return `...Tady najdete odkazy na to, co nás v životě inspirovalo...
          ._._.....#........#...........__×........#....*..._.__...#__
          ..___.......&___XXX........./..-.......#...\--____...._///..
          ...__#....o_en....talos-principle.jpg.........**__.&&/....._
          ..#...X..__*_......________........×~×.....*_.\--____..__×..
          .#.....--._...#...\--____...._//._..scena.jpg........._..&__
          .....&___XXX........./..-........&___XXX........./..-...--_>
          .#...\--____...._/.___.......&...--._...#...\--____...._....
          .--._...#..--._...#.pro*y_sing3D_.....#...___XX___XX.&_.&_..
          ........./..-.......__#............#......__×..__×..\--____.
`;
        }

        if (this.commandMatches(command, 'napoveda')) {
    	    return `Možné příkazy:
  info                  základní informace o svatbě
  dotazník              přístup ke svatebnímu dotazníku
  ubytování             informace o možnostech ubytování
  auto                  informace o autodopravě na místo svatby
  bus                   informace o hromadné dopravě
  program               informace o programu svatby
  příběhy               informace o tématu svatby
  jídlo                 informace o zajištění jídla a pití
  fotky                 naše oblíbené fotky ze společných zážitků
  ukaž [název souboru]  otevře požadovaný soubor
  open [název souboru]  alternativní příkaz k 'ukaž'
  inspirace             odkazy na některé věci, které nám byly inspirací
  hra                   pod tímto příkazem pravděpodobně brzy najdete textovou hru...
  ahoj                  pozdrav počítač
  pozvánka              zobrazí oficiální pozvánku na naši svatbu
  kontakt               zobrazí kontakt na nás
  nápověda              zobrazí tuto nápovědu
  pomoc | help          rada k používání terminálu
`;
        }

        if (command === 'kontakt') {
          return `
  Kontakt na nás:
    rozpletenypribeh[at]protonmail[tečka]com

  Kontakt na svědkyni nevěsty pro případ, že chcete řešit něco,
  o čem nevíme nebo nemáme vědět, a nebo nám třeba chcete připravit
  překvapení do programu:
    [REDACTED]

  Kontakt na naši svatební organizátorku v případě, že např. bloudíte:
    [REDACTED]

`;
        }

        if (command === 'proxy singed' || command === 'PROXY SINGED' || command === 'Proxy Singed' || command === 'proxy_singed' || command === 'PROXY_SINGED' || command === 'Proxy_singed' || command === 'Proxy_Singed') {
          return `  Vzpomínka na dobu, která pominula...
  'open singed.gif'
`;
        }

        if (command === 'help' || command === 'pomoc' || command === 'pomoc | help') {
          return `  Zdá se, že hledáte pomoc s terminálem.
  Používá se tak, že do textového pole píšete různé příkazy,
  které potvrzujete stisknutím klávesy ENTER -
  podobně jako to můžete dělat na běžném počítači.

  Pro seznam dostupných příkazů můžete napsat 'nápověda'.

  Pokud však budete dobře hledat, možná narazíte i na nějaké příkazy,
  které v nápovědě nejsou uvedené.
`;
        }

        if (command === 'hra') {
            return `
  Děkujeme za zájem zahrát si náš digitální minigamebook! Obsahuje nejrůznější reference na věci, které máme rádi a
  které nás baví, můžete tedy zkusit, kolik odkazů poznáte.
  
  Jak to funguje:
  1) Veškeré ovládání probíhá tak, že napíšete odpovídající příkaz (číselný kód či slovo na začátku řádku, který
     označuje danou možnost) do terminálu a stisknete ENTER.
  2) Hra obsahuje více příběhových linií - můžete ji proto hrát vícekrát, abyste prošli i linie, ke kterým se vaše
     postava napoprvé nedostala. Stačí napsat znovu příkaz 'hra' nebo 'start'.
  3) Všechny linie nicméně vedou ke stejnému konci, takže se nemusíte cítit ochuzeni o příběh, pokud hru odehrajete
     jenom jednou.
  4) Hra je také propojená s různými úkoly umístěnými přímo tady na Kačeří - až na ně v textu narazíte, věříme, že to
     bude jasné.
     (Webová verze je přizpůsobená tomu, abyste nemuseli fyzicky jezdit na Kačeří. :) Ačkoli to doporučujeme,
     je to hezké místo!)
  5) Projít celou hru od začátku do konce může trvat rámcově od 15 do 45 minut. Záleží, jakou cestou se vydáte.
     Kdykoli ji ale můžete přerušit a později pokračovat. Stačí si zapamatovat příkaz, kterým jste skončili.
  
  Užijte si to!
  
  ---
  start - Začít novou hru.

`;
        }

        if (command === 'start') {
            return `
  Na jednom zapadlém místě žil malý Unravelík. A ten měl starosti. Trápilo ho, že v lese za jeho paloučkem žije
  zvířátko, které bývá občas smutné. Unravelík ale věděl, že by ho mohl dokázat rozveselit.
  Proto se jednoho dne rozhodl, že se vydá na cestu za poznáním toho, jak pomoci zvířátku, aby nebylo tak smutné.
  Zabalil si zavazadlo na několikadenní výlet (kdoví, kam ho tahle cesta zavede!) a připravil se vyrazit.
  Záhy si však uvědomil jednu potíž. Kam by se měl vydat nejdříve, když se chce naučit, jak někoho rozveselit?
  
  ---
  856 - Do Antických rozvalin, ve kterých prý člověk pozná sám sebe.
  157 - Na Slunečnou paseku, odkud je vidět do celého kraje.
  970 - Na Západní pěšinu, která prý vede každého někam jinam.

`;
        }

        if (command === '126') {
            return `
  Unravelík papír pečlivě uložil tam kde ho našel. Bude lepší, když se nebude plést do věcí jiných lidí, on sám by
  to koneckonců taky neměl rád, kdyby mu někdo prohlížel jeho věci.
  A koneckonců, měl přece na dnešek nějaký úkol! Měl by se vrátit do Antických rozvalin, když teď vyrazí, měl by to
  stihnout ještě před setměním.

  ---
  344 - Vyrazit na cestu do Antických rozvalin.  

`;
        }

        if (command === '128') {
            return `
  Unravelík sestoupil z kopce na druhou stranu vedoucí k majáku. Bylo zajímavé, že tady se pláž začala měnit:
  místo samého písku na ní najednou byly mušle, občas malé kamínky, větvičky a další věci, které zřejmě vyplavilo moře.
  Brzy se mu podařilo dostat se až k majáku, i když se chvílemi musel brodit... Příliv nějak stoupal.
  Vylezl přes pár balvanů, přeskočil nízkou zídku a stanul pod tyčící se věží majáku. Zapadající slunce bylo pořád
  stejně vysoko, což už Unravelíkovi skoro ani nepřišlo divné.

  Ovšem ve chvíli, kdy začal stoupat po schodech ke dveřím majáku, se něco začalo dít.
  Světlo jako by se začalo měnit... Unravelík se znovu podíval směrem ke slunci. S každým schodem, o který vystoupal,
  se slunce blížilo k obzoru. A s každým schodem, který sestoupil, se od obzoru zase vzdálilo.
  To... Bylo skutečně zvláštní. Zdálo se, že tenhle maják - nebo přesněji Unravelíkova činnost okolo majáku - určovala
  pohyb času. Ale jakmile ze schodů sestoupil úplně, slunce se vrátilo do původní polohy nad obzorem.
  
  Unravelík nevěděl, co si počít. Nevěděl, jak se sem dostal; nevěděl, kde vlastně je; nevěděl ani, kolik je hodin,
  protože slunce přestalo fungovat normálně; a ještě ke všemu měl na dnešek nějaký program, a tohle ho od něj úplně
  rozptýlilo.
  Takhle by to nešlo, řekl si. Teď půjde do toho majáku, slunce neslunce, a zjistí co a jak.
  
  Unravelík tedy vystoupal po schodech až ke vstupu do majáku. Slunce bylo přesně napůl schované za obzorem.
  
  Na dveřích bylo něco napsané.
  
  ---
  626 - Přečíst nápis na dveřích.

`;
        }

        if (command === '157') {
            return `
  Slunečnou paseku měl Unravelík vždycky rád. Nejlepší je se na ní jen tak natáhnout a koukat na mraky. Mraky jsou
  vždycky nejzajímavější, když kolem nich svítí slunce, a na Slunečné pasece slunce svítilo skoro pořád.
  Cesta k ní vedla skrze hustý les plný mladých i starých stromů, mezi kterými se Unravelík dokázal sotva protáhnout.
  Měl co dělat, aby se nezamotal do pavučin nebo si nepotrhal nitky o nízké větve. Nakonec se ale přece jen vymotal
  z lesa a stanul na okraji Slunečné paseky.
  Slunce tam skutečně svítilo, ale... Na pasece byl ještě někdo další. Nebo se to Unravelíkovi zdá...?
  Zkusil zavolat na pozdrav. Postava se podívala jeho směrem, pak ale seskočila z pařezu, na kterém seděla, a schovala
  se mu z dohledu.
  Unravelík spěchal směrem k ní, když ale dorazil k pařezu, po postavě jako by se slehla zem. Nebyla nikde vidět,
  ale přitom nemohla stihnout utéct tak rychle!
  Unravelík chvíli přemýšlel, co s tím bude dělat, ale nakonec se rozhodl to nechat být. Vzpomněl si, kam měl původně
  namířeno... Přece do Antických rozvalin! Jak se stalo, že na rozcestí zapomněl odbočit?
  No, ale když už je tady, tak by si mohl na chvilku odpočinout a pokračovat za chvíli.
  Unravelík se natáhl na trávu a zahleděl se na mraky.
  
  Co bude dělat dál?
  
  ---
  546 - Na chvilku si příjemně zdřímne.
  625 - Vyrazí zpět a na rozcestí půjde do Antických rozvalin.
  839 - Znovu zkusí zjistit, kam zmizela tajemná postava.

`;
        }

        if (command === '203') {
            return `
  Ne že by tomu dával velké naděje, ale Unravelík si často říkával, že kdyby člověk u ničeho nevydržel, jakmile se
  setká s prvními překážkami, tak by máloco v životě dokončil.
  Bohužel se brzy ukázalo, že tohle není jedna z těch věcí, které by se daly překonat pouze vytrvalostí.
  Za chvíli totiž zase došel zpět na stejné místo. Teď už si byl jistý, že ho ten ostrov nutí chodit v kruzích.
  
  Dobře, tak může zkusit něco jiného.
  
  ---
  596 - Zkusí se vydat doleva.
  828 - Zkusí zůstat chvíli na místě. Kdoví, třeba se něco stane...?

`;
        }

        if (command === '228') {
            return `
  Jak tak Unravelík šel, tak vnímal, že ostrov se úplně mírně stáčí dokola... Pravděpodobně nebyl moc velký.
  Mezitím uvažoval o tom, že to opravdu nedává velký smysl, že se člověk zničehonic ocitne na místě, které nejenom že
  nezná, ale ani neví jak se na něj dostal.
  V tom bude skoro určitě něco víc než že by se jenom zamyslel a přestal na chvíli vnímat zem pod nohama.
  
  Ale možná že když se to takhle stalo, tak třeba se to... mělo stát? Pokud je osud skutečný, tak si Unravelík
  představoval, že přesně něco takového by lidem dělal.
  
  Zrovna jak se tyhle myšlenky Unravelíkovi míhaly v hlavě, tak si všiml malého seschlého stromku kousek od pláže.
  Přišel k němu blíž, podíval se směrem k moři... A uvědomil si, že musel obejít celý ostrov.
  Protože to byl přesně ten samý stromek, a Slunce bylo přesně ve stejném úhlu jako předtím.
  
  Ale jak to bylo možné? To na tenhle ostrov nevedla žádná cesta ani na něm nebyl žádný přístav nebo aspoň molo,
  kde by mohly kotvit lodě?
  Ať tak či tak, odpověď na svoje otázky asi bude Unravelík muset hledat dál.
  
  ---
  979 - Tentokrát se zkusí vydat doleva.
  203 - Zkusí pokračovat ještě doprava.
  467 - Zkusí zůstat chvíli na místě. Kdoví, třeba se něco stane...?

`;
        }

        if (command === '240') {
            return `
  Jak tak Unravelík šel, tak vnímal, že ostrov se úplně mírně stáčí dokola... Pravděpodobně nebyl moc velký.
  Mezitím uvažoval o tom, že to opravdu nedává velký smysl, že se člověk zničehonic ocitne na místě, které nejenom že
  nezná, ale ani neví jak se na něj dostal.
  V tom bude skoro určitě něco víc než že by se jenom zamyslel a přestal na chvíli vnímat zem pod nohama.
  
  Ale možná že když se to takhle stalo, tak třeba se to... mělo stát? Pokud je osud skutečný, tak si Unravelík
  představoval, že přesně něco takového by lidem dělal.
  
  Zrovna jak se tyhle myšlenky Unravelíkovi míhaly v hlavě, tak si všiml malého seschlého stromku kousek od pláže.
  Přišel k němu blíž, podíval se směrem k moři... A uvědomil si, že musel obejít celý ostrov.
  Protože to byl přesně ten samý stromek, a Slunce bylo přesně ve stejném úhlu jako předtím.
  
  Ale jak to bylo možné? To na tenhle ostrov nevedla žádná cesta ani na něm nebyl žádný přístav nebo aspoň molo,
  kde by mohly kotvit lodě?
  Ať tak či tak, odpověď na svoje otázky asi bude Unravelík muset hledat dál.
  
  ---
  979 - Tentokrát se zkusí vydat doleva.
  203 - Zkusí pokračovat ještě doprava.
  467 - Zkusí zůstat chvíli na místě. Kdoví, třeba se něco stane...?

`;
        }

        if (command === '262-hint') {
            return `
  Ten papír, který jste četli, zněl jako že ho musel psát někdo docela chytrý. Zkuste se zeptat na baru,
  jestli o tomhle něco nevědí.
  A dejte si tam něco k pití, to pivo se nevypije samo!

  Nicméně... Pokud si opravdu, opravdu nevíte rady... Můžete zkusit kliknout na poslední tečku v nadpisu.
  Třeba díky tomu najdete, co hledáte.
  
  A nebo samozřejmě vždycky můžete do okna terminálu napsat '262-řešení'. ;)

`;
        }

        if (command === '279') {
            return `
  Unravelík šel dlouho. Věděl, že Antické rozvaliny jsou od jeho obydlí daleko, ale stejně ho překvapilo, že se
  skoro setmělo, než dorazil k pozůstatkům hradeb. A to je přitom teprve konec léta, žádná zima, kdy by dny byly
  krátké jako bývají!
  Ale nakonec si řekl, že to vlastně ničemu nevadí, má koneckonců sbaleno i na přespání pod širým nebem.
  Vždyť není kam spěchat. Ostatně při poznávání sebe sama by člověk spěchat ani neměl, protože právě tak mu může
  uniknout to nejdůležitější.
  Unravelík se tedy zhluboka nadechl, a pak se vydal do rozvalin.
  
  Netrvalo dlouho, než narazil na zvláštní věc. Přímo uprostřed malého náměstíčka pod rozkvetlým akátem byla
  knihovna, a plná knih!
  Vypadala, jako by tam stála odjakživa, ale to přitom nemohla být pravda... Unravelík přistoupil blíž a zkoumavě
  se na ni podíval. A po chvilce si všiml, že knihovna má na své vrchni straně něco napsané vybledlou křídou:
  
  "Najdi mě ve svém světě."
  
  Hmm. Tohle je poněkud zvláštní, řekl si Unravelík, vždyť já tady jsem ve svém světě. Ale možná přijde na to,
  co to znamená... Začal si tedy prohlížet hřbety knížek, jestli mu některý nenapoví, jak pokračovat.
  A skutečně, zdálo se, jako by jedna z knížek do knihovny nepatřila. Když ji Unravelík vytáhl, aby se do ní
  podíval, tak se nepatrně, ale přesto znatelně zachvěla. A když ji otevřel, zjistil, že po okrajích má na
  některých stránkách napsané nějaké poznámky!
  Tohle nemůže být jen tak, pomyslel si. Posadil se ke stolku s lampou, který byl hned vedle knihovny.
  A začetl se do knihy... A do poznámek po okrajích stránek.
  
  279-hint - Pokud si nejste jistí, co dělat dál, napište tento příkaz.

`;
        }

        if (command === '279-hint') {
            return `
  Někde na Kačeří najdete knihovnu podobnou té, jakou popisuje příběh. V ní se ukrývá další postup příběhu -
  možná právě v těch poznámkách.
  
  ...
  
  ...
  
  ...
  
  Něco nám říká, že ty správné úryvky z knih v knihovně na Kačeří by mohly být uvedeny i pod příkazem 'hovory'.
  
  ...
  
  A nebo taky můžete zkusit napsat '279-řešení', pokud zrovna nemáte náladu na antické čtení, že ano. :)

`;
        }

        if (command === '344') {
            return `
  Unravelík šel dlouho. Věděl, že Antické rozvaliny jsou od jeho obydlí daleko, ale stejně ho překvapilo, že se
  skoro setmělo, než dorazil k pozůstatkům hradeb. A to je přitom teprve konec léta, žádná zima, kdy by dny byly
  krátké jako bývají!
  Ale nakonec si řekl, že to vlastně ničemu nevadí, má koneckonců sbaleno i na přespání pod širým nebem.
  Vždyť není kam spěchat. Ostatně při poznávání sebe sama by člověk spěchat ani neměl, protože právě tak mu může
  uniknout to nejdůležitější.
  Unravelík se tedy zhluboka nadechl, a pak se vydal do rozvalin.
  
  Netrvalo dlouho, než narazil na zvláštní věc. Přímo uprostřed malého náměstíčka pod rozkvetlým akátem byla
  knihovna, a plná knih!
  Vypadala, jako by tam stála odjakživa, ale to přitom nemohla být pravda... Unravelík přistoupil blíž a zkoumavě
  se na ni podíval. A po chvilce si všiml, že knihovna má na své vrchni straně něco napsané vybledlou křídou:
  
  "Najdi mě ve svém světě."
  
  Hmm. Tohle je poněkud zvláštní, řekl si Unravelík, vždyť já tady jsem ve svém světě. Ale možná přijde na to, co
  to znamená... Začal si tedy prohlížet hřbety knížek, jestli mu některý nenapoví, jak pokračovat.
  A skutečně, zdálo se, jako by jedna z knížek do knihovny nepatřila. Když ji Unravelík vytáhl, aby se do ní
  podíval, tak se nepatrně, ale přesto znatelně zachvěla. A když ji otevřel, zjistil, že po okrajích má na
  některých stránkách napsané nějaké poznámky!
  Tohle nemůže být jen tak, pomyslel si. Posadil se ke stolku s lampou, který byl hned vedle knihovny.
  A začetl se do knihy... A do poznámek po okrajích stránek.
  
  ---
  344-hint - Pokud si nejste jistí, co dělat dál, napište tento příkaz.

`;
        }

        if (command === '344-hint') {
            return `
  Někde na Kačeří najdete knihovnu podobnou té, jakou popisuje příběh. V ní se ukrývá další postup příběhu -
  možná právě v těch poznámkách.
  
  ...
  
  ...
  
  ...
  
  Něco nám říká, že ty správné úryvky z knih v knihovně na Kačeří by mohly být uvedeny i pod příkazem 'hovory'.
  
  ...
  
  A nebo taky můžete zkusit napsat '344-řešení', pokud zrovna nemáte náladu na antické čtení, že ano. :)

`;
        }

        if (command === '353') {
            return `
  Unravelík si jenom pozvdechl, co je to dneska za lidi, oprášil si kalhoty a pomalu zase vyrazil.
  Beztak to je ale divné, vidět tady někoho běhat, to se opravdu musí dít něco mimořádného. Co si Unravelík pamatoval,
  tak tady vždy potkával jenom zamyšlené, pomalu chodící lidi. Žádné takové neomalené osoby, které kolem vás
  proběhnou, vyděsí vás a ještě vás porazí na zem!
  
  Takové myšlenky se mu honily hlavou, až nakonec dorazil až doprostřed rozvalin na hlavní náměstí. To už byl zase
  téměř tak klidný jako předtím a se zájmem se rozhlížel, jestli někoho neuvidí.
  Postavy tam nebyly žádné, aspoň tam kam měl Unravelík výhled. Vysoké, poměrně zachovalé budovy vytvářely moc
  zajímavý pohled a probouzely v člověku pocit, že se nachází na velmi významném místě.
  Byla tam ale jedna zvláštní věc, které si Unravelík všiml až po chvíli...
  Jedny postranní dveře do nejvyššího domu na celém náměstí byly vyplněné jakousi šedou mlhou.
  
  Co by to mohlo znamenat?
  
  No... Když se nad tím Unravelík zamyslel... Tohle bylo přece to, co Antické rozvaliny vždycky dělaly. ČLověk měl
  pocit, že do nich jde za něčím, a pak se stalo něco zcela neočekávaného, co ho často úplně rozhodilo.
  Ale nakonec, když se nechal unášet proudem událostí, zjistil, že se dostal přesně k tomu, za čím původně přišel.
  
  ---
  617 - Takže asi dává smysl, aby vstoupil dovnitř, že? A uvidí, co se stane dál...

`;
        }

        if (command === '398') {
            return `
  Unravelík se rozběhl k lesu. Tam by mu ta postava snadno mohla zmizet z dohledu a ztratit se tady v lese je to
  poslední, co by zrovna teď potřeboval. Kdoví kdy by se potom dostal do Antických rozvalin!
  Na hranici lesa zpomalil, aby příliš nepolámal křoví, a opatrně prošel skrz. Les tady byl nečekaně hustý, takže mu
  chvilku trvalo, než si zvykl na přítmí.
  Pak ale zahlédl tajemnou postavu, jak na něj mává o kus dál a jde někam ze svahu.
  Unravelík se tedy vydal za ní. Šli takhle nějakou dobu, měl co dělat aby jí stačil.
  A jak Unravelík spěchal, tak si nevšiml, že se blíží k průrvě v zemi.
  
  Když mu uklouzla noha a on zahučel přímo do té průrvy, ani nestihl překvapeně vykřikout.
  A pak už se řítil jak po skluzavce dolů, snažil se zachytit nějakých kořenů a větví, aby zpomalil svůj pád, ale
  bylo mu to málo platné.
  Jak rychle ovšem nepříjemný sešup začal, tak taky skončil - Unravelík spadl do hromady spadaného listí, takže se
  mu ani nic vážného nestalo.
  
  Rozladěně a s nadávkami, jaké od něj málokdo slyšel, si ometal nohavice i rukávy a tak mu chvíli trvalo, než si
  všiml, že se ocitl přímo před jakousi starou chalupou.
  Měla temná okna, z okapů a střechy místy visel břečťan, dům měl spoustu věžiček a výstupků a na verandě bylo
  starodávné houpací křeslo... Všechno vypadalo, jako by to tady bylo odjakživa.
  To Unravelíka ale překvapilo.
  Co tady uprostřed lesa dělá takováhle chalupa, pomyslel si? A jakto, že ji tu nikdy neviděl? Tenhle les přece
  znal skoro jako svoje boty...
  
  Co Unravelík udělá?
  
  ---
  681 - Zkusí zaklepat na přední dveře. Jeden nikdy neví!
  585 - Zkusí obejít chalupu kolem dokola, jestli nemá další vstup nebo něco jiného zajímavého.

`;
        }

        if (command === '402') {
            return `
  Moře tam skutečně pořád znělo! Byl to moc příjemný zvuk, jako nekonečné vlny narážející pořád dokola na břeh.
  Bylo v tom něco zvláštně ujišťujícího, že ať se stane cokoli, tak vlny moře tady budou dál, bez ustání a neúnavně
  budou dorážet na útesy a břehy po celém světě... A odtamtud budou slyšet třeba i tady, v dutině Vykotlaného stromu.
  Unravelík se zhluboka nadechl, vydechl... a vtom si všiml, že u kořenů stromu je něco položené.
  Byl to nějaký papír...? Co s ním Unravelík bude dělat?
  
  ---
  347 - Prohlédne si, co je na papíře napsáno.
  126 - Raději papír nebude prohlížet a vrátí ho zpět.

`;
        }

        if (command === '405') {
            return `
  Přední dveře skoro nikdy nejsou ty, kterými se člověk dostane do neznámého domu... Aspoň podle filmů, které
  Unravelík sledoval. Vydal se tedy okolo, aby viděl, jestli třeba nenajde otevřené okno nebo zadní vchod.
  Otevřená okna sice na žádné straně neobjevil, zato zadní vchod u domu byl. Ale byl zavřený podobně jako ten přední.
  Zklamaně od dveří poodstoupil... A málem spadl na zadek, jak zakopl o kámen, o kterém by přísahal, že tam ještě
  před chvílí nebyl.
  Vypadalo to, že k tomu kameni je něco přivázané... Že by kus papíru?
  
  ---
  903 - Unravelík se pro něj sehnul a začal ho číst.

`;
        }

        if (command === '409') {
            return `
  Trvalo nějakou dobu, než si Unravelíkovy oči zvykly na přítmí. Pomalu se vypotácel z ponorky, pořád se mu ještě
  motala hlava z rychlého pohybu oblohy.
  První co udělal bylo, že šel k oknu, aby se podíval na nebe - byla už noc, ale obloha se podle všeho chovala
  normálně a les byl taky povědomý. Asi ani nebude tak daleko od domova, oddechl si.
  
  Unravelík se tedy otočil k východu z místnosti, aby vyrazil domů a pořádně se z toho všeho vyspal.
  Když ovšem vyšel z místnosti, tak si všiml namodralého světla vycházejícího z místnosti na konci chodby.
  Po všech těch podivnostech uplynulého dne už měl sice Unravelík všeho pokrk, ale tohle vypadalo skutečně zvláštně
  a popravdě... kdyby tam nešel, tak by si stejně nedokázal pomoct a musel by o tom přemýšlet.
  Lepší to vzít po hlavě a nemít žádné nevyjasněné pochybnosti! A třeba zjistí, kam se to vlastně dostal a co to
  mělo znamenat s tím mořem.
  
  ---
  871 - Zhluboka se nadechnout a vstoupit do místnosti s namodralým světlem.

`;
        }

        if (command === '434') {
            return `
  Postava byla zčásti schovaná za keřem, což Unravelíka poněkud znervózňovalo. Přišel nakonec ale až skoro k ní,
  když mu gestem ukázala, aby se zastavil. Unravelík tak učinil a čekal, co z toho bude.
  Pak mu tajemná postava naléhavým šeptem sdělila:
  
  Nazdar Unravelíku. Nemám bohužel moc času, takže to vezměme rychle. Dnes budu potřebovat tvoji pomoc s jednou věcí.
  Je to na delší povídání, ale na to snad bude čas později, když všechno vyjde jak má. Zatím bude stačit, abys
  pokračoval támhle tudy - ukáže na jednu z cest z křižovatky - a věřil mi. A teď mě prosím omluv, běžím zařídit
  ještě jednu nutnou záležitost. Zatím!
  
  S těmi slovy se osoba zničehonic otočila a než se Unravelík stihl nadechnout, aby něco řekl, tak zmizela v uličce
  mezi dalšími domy.
  Unravelík byl tak zmatený, jako už dlouho ne. A navíc nevěděl, co si má teď myslet o svých pocitech, v jednu chvíli
  si prohlíží hvězdy na obloze a teď se tady honí za někým, koho ani nezná!
  Ale když se nad tím zamyslel... Tak tohle vlastně bylo přesně to, v čem Antické rozvaliny člověka nikdy nezklamaly.
  Vždycky se stalo něco neočekávaného... A vždycky to nakonec vedlo tam, kam si člověk původně přál.
  Takže vlastně proč ne, proč by té podivné osobě neměl věřit? Co by se mohlo pokazit, řekl si Unravelík. 
  
  ---
  884 - ...A s novým nadšením se vydal po cestě, kterou mu ukázala.

`;
        }

        if (command === '467') {
            return `
  Jak tak Unravelík seděl a přemýšlel, postupně se mu podařilo si vybavit zvuk moře, který slýchával z Vykotlaného
  stromu. Tohle skutečně působilo, jako že to je to stejné moře... Ale jak je možné, že se ocitl přímo u něj? Vždyť
  ten zvuk měl být jenom zvuk... Nebo se snad opravdu v dutině stromu nacházel celý malý svět?
  Ale to je přece nesmysl... Nebo o něčem takovém Unravelík alespoň nikdy neslyšel.
  Dobře, tak řekněme, že se tedy skutečně dostal nějakým způsobem na místo, které je zdrojem zvuku Vykotlaného stromu.
  Pokud se dostal sem, musí existovat i cesta zpátky.
  Ale kudy by tak mohla vést? Unravelík se znovu podíval doleva a doprava, jestli neuvidí alespoň něco... Ale ani na
  jedné straně nic nebylo, kromě pláže a mírného kopce vedoucího někam do nitra ostrova.
  Zadíval se přímo před sebe... Pořád tam bylo zapadající slunce. Skoro to vypadalo, jako by se vůbec nepohnulo...
  Počkat.
  Ono se skutečně vůbec nehýbe. Unravelík už tam seděl nějakou chvíli, za tu dobu by si musel všimnout, že se poloha
  slunce aspoň trochu změnila!
  Dobře, pomyslel si. Něco tady pěkně nehraje, ale když se to vezme z té dobré stránky, aspoň by Unravelík neměl
  přijít o moc času v reálném světě, když bude tady a zkusí tomu přijít na kloub.
  Vstal, protáhl se a podíval se postupně doleva i doprava podél pláže.
  
  Nakonec nahmátl něco malého v kapse a rozhodl se, že si usnadní práci s vybíráním, kudy půjde:
  
  Když padne panna, tak půjde doleva.
  Když padne orel, půjde doprava.
  
  Pak vyhodil korunu do vzduchu.
  
  Co Unravelíkovi padlo?
  
  ---
  994 - Panna.
  240 - Orel.

`;
        }

        if (command === '519') {
            return `
  Vylezl přes pár balvanů, přeskočil nízkou zídku a stanul pod tyčící se věží majáku. Zapadající slunce bylo pořád
  stejně vysoko, což už Unravelíkovi skoro ani nepřišlo divné.
  
  Ovšem ve chvíli, kdy začal stoupat po schodech ke dveřím majáku, se něco začalo dít.
  Světlo jako by se začalo měnit... Unravelík se znovu podíval směrem ke slunci. S každým schodem, o který vystoupal,
  se slunce blížilo k obzoru. A s každým schodem, který sestoupil, se od obzoru zase vzdálilo.
  To... Bylo skutečně zvláštní. Zdálo se, že tenhle maják - nebo přesněji Unravelíkova činnost okolo majáku - určovala
  pohyb času. Ale jakmile ze schodů sestoupil úplně, slunce se vrátilo do původní polohy nad obzorem.
  
  Unravelík nevěděl, co si počít. Nevěděl, jak se sem dostal; nevěděl, kde vlastně je; nevěděl ani, kolik je hodin,
  protože slunce přestalo fungovat normálně; a ještě ke všemu měl na dnešek nějaký program, a tohle ho od něj úplně
  rozptýlilo.
  Takhle by to nešlo, řekl si. Teď půjde do toho majáku, slunce neslunce, a zjistí co a jak.
  
  Unravelík tedy vystoupal po schodech až ke vstupu do majáku. Slunce bylo přesně napůl schované za obzorem.
  
  Na dveřích bylo něco napsané.
  
  ---
  626 - Přečíst nápis na dveřích.

`;
        }

        if (command === '546') {
            return `
  Unravelíkovi se oči zavřely skoro samy, jakmile se trochu zamyslel. Ani si toho nevšiml, ale muselo utéct více než
  půl hodiny. Takové zdřímnutí po odpolední procházce ale snad ještě nikomu neublížilo, takže ničeho nelitoval. 
  Posadil se a zamžoural do kraje.
  Všechno bylo stejně jako dřív, snad jen slunce se na obloze trochu posunulo... Ale pocit měl zvláštní. Jako by ho
  někdo pozoroval. Takové to mírné šimrání v zádech, které ani není cítit, ale přesto o něm člověk ví.
  Unravelík se tak mimochodem otočil a zadíval se za sebe. A skutečně, viděl zase tu stejnou postavu, jak na něj
  kouká zpoza stromu na hranici lesa! V tuhle chvíli vyskočil a rozběhl se k ní.
  Chtěl se s ní aspoň pozdravit, když už spolu hrají na takovouhle schovávanou. A taky ho celá ta situace trochu
  znervózňovala, takže chtěl zjistit, kdo to vlastně je.
  Jakmile tajemná postava viděla, že se Unravelík rozbíhá jejím směrem, tak se otočila a začala utíkat.
  Unravelík zrychlil, už i viděl, že postava je na poměry běžných lidí docela maličká. Zkusil na ni zavolat, ať na
  něj počká, ale postava ani nezpomalila. 
  Po malé chvilce náhle zmizela do křoví napravo od cesty. Unravelík běžel za ní, ale postava mu zase zmizela doslova
  před očima!
  Nicméně tentokrát Unravelíkovu pozornost upoutalo ještě něco jiného. Přímo před ním stála veliká dřevěná chalupa,
  kterou si nevybavoval.
  Měla temná okna, z okapů a střechy místy visel břečťan, dům měl spoustu věžiček a výstupků a na verandě bylo
  starodávné houpací křeslo... Všechno vypadalo, jako by to tady bylo odjakživa.
  A taky že v chalupě už dlouho nikdo nebydlel, působila celkově dost zanedbaně.
  Že by ta postava zmizela dovnitř?
  
  ---
  681 - Zkusit zaklepat na přední dveře.
  585 - Obejít chalupu kolem dokola, jestli nemá další vstup nebo něco jiného zajímavého.

`;
        }

        if (command === '585') {
            return `
  Přední dveře skoro nikdy nejsou ty, kterými se člověk dostane do neznámého domu... Aspoň podle filmů, které
  Unravelík sledoval. Vydal se tedy okolo, aby viděl, jestli třeba nenajde otevřené okno nebo zadní vchod.
  Otevřená okna sice na žádné straně neobjevil, zato zadní vchod u domu byl. Ale byl zavřený podobně jako ten přední.
  Zklamaně od dveří poodstoupil... A málem spadl na zadek, jak zakopl o kámen, o kterém by přísahal, že tam ještě
  před chvílí nebyl.
  Vypadalo to, že k tomu kameni je něco přivázané... Že by kus papíru?
  
  ---
  903 - Unravelík se pro něj sehnul a začal ho číst.

`;
        }

        if (command === '596') {
            return `
  Unravelík šel, ani už nevěděl jak dlouho. Musela to být alespoň hodina chůze pískem a vlnami - zřejmě byl zrovna
  příliv. Bylo to ale vlastně docela příjemné, když se to tak vzalo.
  Zapadající slunce stále hřálo, ale už nespalovalo. A navíc vytvářelo krásné světlo, přesně takové, o kterém mluví
  všichni fotografové, když uděláte tu chybu a zeptáte se jich.
  Unravelík zrovna stoupal do kopce, písek tady zřejmě vytvořil jakousi dunu zvedající se napříč pláží, takže přes ni
  nebylo vidět.
  Nakonec stanul na vrcholu duny.
  A před sebou uviděl maják! Vysoký, červenobílý, na kamenitém výběžku, zkrátka přesně jak z knížek.
  Pořádně si oddechl, protože kde byla civilizace, tam museli být lidé. A kde byli lidé, odtamtud se muselo jít
  dostat nějakými konvenčními cestami.
  
  Kudy se Unravelík k majáku vydá?
  
  ---
  128 - Nejkratší cestou přímo podél pláže.
  889 - Po cestě uhýbající trochu do vnitrozemí - přímo na ní totiž stojí ukazatel s nápisem "K MAJÁKU".

`;
        }

        if (command === '607') {
            return `
  Unravelík zakroutil hlavou, pokrčil rameny a měl se k odchodu.
  Alespoň tak si to představoval.
  Jak se však otočil, tak opravdu nečekal, že ho v tu chvíli zasáhne do hlavy jablko.
  
  Otočil se zase zpátky, skoro rudý vzteky.
  Dorázoval k postavě v křoví a už už si chtěl začít vyříkávat, co to má přesně znamenat, ale postava k němu
  přiskočila a zakryla mu pusu rukou. Unravelík byl z toho tak vyvedený z míry, že se nezmohl na odpor.
  Postava mu naléhavým šeptem sdělila:
  
  Nazdar Unravelíku. Nemám bohužel moc času, takže to vezměme rychle. Dnes budu potřebovat tvoji pomoc s jednou
  věcí. Je to na delší povídání, ale na to snad bude čas později, když všechno vyjde jak má. Zatím bude stačit,
  abys pokračoval támhle tudy - ukáže na jednu z cest z křižovatky - a věřil mi. A teď mě prosím omluv, běžím
  zařídit ještě jednu nutnou záležitost. Zatím!
  
  S tím se osoba zničehonic otočila a než se Unravelík stihl nadechnout, aby něco řekl, tak zmizela v uličce mezi
  dalšími domy.
  Unravelík byl tak zmatený, jako už dlouho ne. A navíc nevěděl, co si má teď myslet o svých pocitech, v jednu
  chvíli si prohlíží hvězdy na obloze a teď tady má plnit úkoly od někoho, koho ani nezná!
  Ale když se nad tím zamyslel... Tak tohle vlastně bylo přesně to, v čem Antické rozvaliny člověka nikdy
  nezklamaly. Vždycky se stalo něco neočekávaného... A vždycky to nakonec vedlo tam, kam si člověk původně přál.
  Takže vlastně proč ne, proč by té podivné osobě neměl věřit? Co by se mohlo pokazit, řekl si Unravelík. 
  
  ---
  884 - ...A s novým nadšením se vydal po cestě, kterou mu ukázala.

`;
        }

        if (command === '608') {
            return `
  Po chvilce opatrného našlapování se Unravelík přikradl ke dveřím, na škvírku je otevřel a podíval se, co je za
  nimi. Viděl akorát další ztemnělou místnost, možná nějakou chodbu... Bylo tam taky schodiště vedoucí někam do
  vyšších pater.
  Váhavě otevřel dveře tak, aby se mohl protáhnout skrz. Když vešel do chodby, tak si nevšiml ničeho zvláštního,
  co by bylo jinak ve srovnání s předchozí místností, až na slabé namodralé světélkování, které šlo z horního patra
  a od schodiště.
  
  Unravelík se opatrně plížil domem a nahoru po schodišti. V duchu si říkal, do čeho se to zase dostal. Až tohle
  bude vyprávět přátelům - doufejme že jim to tedy bude vyprávět, že tady nejde o nějakou opravdu nebezpečnou věc!
  Když se dostal na vrchol schodiště, bylo už jasně vidět, že světlo vychází z pootevřených dveří na konci chodby.
  Šel k nim, ale při prvním náznaku nebezpečí byl rozhodnutý se otočit a prchat o život. Být hrdina a pomáhat lidem
  je jedna věc, ale zaplést se do kdovíjakých temných magií nebo co to má být, to Unravelík opravdu zapotřebí neměl.
  
  Došel až ke dveřím a pořád se nic nedělo. Ani neslyšel žádné zvuky, všude bylo naprosté ticho.
  
  ---
  871 - Zhluboka se nadechnout, rozrazit dveře a vkročit do místnosti.

`;
        }

        if (command === '616') {
            return `
  Unravelík věděl, že v rozvalinách se lidé často procházejí a hledají inspiraci... I on sem čas od času zašel a
  jen tak chodil a přemýšlel.
  Díky těm lidem tady vždy byl dostatek světla z loučí, které vždy někdo, kdo šel zrovna okolo a stmívalo se,
  zapálil.
  Dnes měl Unravelík však dojem, že ví proč tu je a že se příliš procházet nemusí. Měl překvapivě čistou hlavu
  a klid a jistotu v duši.
  
  ...
  
  Proto ho poněkud vyvedlo z míry, když ze strany přiběhla nějaká postava a srazila Unravelíka k zemi.
  
  Unravelík se snažil vyhrabat na nohy, aby zjistil co to má být, ale ta postava už mizela ve tmě, sotva se mu
  podařilo zahlédnout červený záblesk... Asi nějakého kusu oblečení.
  Zavolal za ní, ale zpět se nic neozvalo.
  
  Poběží Unravelík za tajemnou postavou?
  
  ---
  959 - Rozběhne se za ní.
  353 - Ne, nebude se přece tolik namáhat. Raději bude volným krokem pokračovat do středu rozvalin.

`;
        }

        if (command === '617') {
            return `
  Projít mlhou dalo nějaké úsilí, bylo to trochu jako tlačit celým tělem proti zdi udělané z medu.
  Nakonec se ale Unravelíkovi podařilo projít skrz. Mlha se za ním hned zase zavřela a Unravelík se ocitl v temné
  chodbě, kterou osvětlovalo pouze světlo roztroušených svíček.
  Vypadalo to tedy pěkně strašidelně, to se muselo nechat.
  Unravelík pomalu vykročil a dával si pozor, aby nepřevrhl žádnou ze svíček.
  
  Šel takhle nějakou dobu, muselo to být aspoň 20 minut. Posléze už zrychlil, vzhledem k tomu, že na něj nic
  nevyskakovalo ani mu žádní kamarádi, kteří by to připravili jako špatný vtip, neudělali nic ošklivého, tak si
  řekl, že nemá cenu se zdržovat.
  Prostě půjde a uvidí, kam tahle chodba vede.
  
  ...
  
  Nakonec došel ke kamennému schodišti vedoucímu někam nahoru. Mírně zatáčelo, takže nebylo vidět až na jeho vrchol.
  Pokrčil rameny. Teď už stejně nemá kam jinam jít, a zpátky se mu tedy nechtělo.
  Začal stoupat po schodišti, až dorazil ke starobyle vypadajícím dveřím. Nevypadalo to, že by měly nějaký zámek...
  Unravelík zkusil vzít za kliku.
  A dveře se se zlověstným skřípěním otevřely! Unravelík by přísahal, že v tom skřípění slyšel zamňoukání a že se
  mu o nohu otřelo něco chlupatého... Byl už teď jak na jehlách.
  
  Opatrně se rozhlédl po místnosti. Byla celá ztemnělá, plná starého nábytku a zatuchlých koberců. Měla docela
  vysoký strop, všechny zdi byly obložené dřevem a kde nebyly knihovny a skříně, tam visely obrazy s nezřetelnými
  malbami.
  Unravelík se musel podzemní chodbou dostat do nějakého starého domu nebo chalupy, ale proč? Zatím moc nechápal,
  co se tady děje. Ale třeba když prozkoumá další části domu, tak na něco přijde.
  
  ---
  608 - přejít místnost a otevřít dveře do další části domu.

`;
        }

        if (command === '625') {
            return `
  Unravelík šel dlouho. Věděl, že Antické rozvaliny jsou od jeho obydlí daleko, ale stejně ho překvapilo, že se
  skoro setmělo, než dorazil k pozůstatkům hradeb. A to je přitom teprve konec léta, žádná zima, kdy by dny byly
  krátké jako bývají!
  Ale nakonec si řekl, že to vlastně ničemu nevadí, má koneckonců sbaleno i na přespání pod širým nebem.
  Vždyť není kam spěchat. Ostatně při poznávání sebe sama by člověk spěchat ani neměl, protože právě tak mu může
  uniknout to nejdůležitější.
  Unravelík se tedy zhluboka nadechl, a pak se vydal do rozvalin.
  
  Netrvalo dlouho, než narazil na zvláštní věc. Přímo uprostřed malého náměstíčka pod rozkvetlým akátem byla
  knihovna, a plná knih!
  Vypadala, jako by tam stála odjakživa, ale to přitom nemohla být pravda... Unravelík přistoupil blíž a zkoumavě
  se na ni podíval. A po chvilce si všiml, že knihovna má na své vrchni straně něco napsané vybledlou křídou:
  
  "Najdi mě ve svém světě."
  
  Hmm. Tohle je poněkud zvláštní, řekl si Unravelík, vždyť já tady jsem ve svém světě. Ale možná přijde na to,
  co to znamená... Začal si tedy prohlížet hřbety knížek, jestli mu některý nenapoví, jak pokračovat.
  A skutečně, zdálo se, jako by jedna z knížek do knihovny nepatřila. Když ji Unravelík vytáhl, aby se do ní
  podíval, tak se nepatrně, ale přesto znatelně zachvěla. A když ji otevřel, zjistil, že po okrajích má na
  některých stránkách napsané nějaké poznámky!
  Tohle nemůže být jen tak, pomyslel si. Posadil se ke stolku s lampou, který byl hned vedle knihovny.
  A začetl se do knihy... A do poznámek po okrajích stránek.
  
  625-hint - Pokud si nejste jistí, co dělat dál, napište tento příkaz.

`;
        }

        if (command === '625-hint') {
            return `
  Někde na Kačeří najdete knihovnu podobnou té, jakou popisuje příběh. V ní se ukrývá další postup - možná
  právě v těch poznámkách.
  
  ...
  
  ...
  
  ...
  
  Něco nám říká, že ty správné úryvky z knih v knihovně na Kačeří by mohly být uvedeny i pod příkazem 'hovory'.
  
  ...
  
  A nebo taky můžete zkusit napsat '625-řešení', pokud zrovna nemáte náladu na antické čtení, že ano. :)

`;
        }

        if (command === '626-hint') {
            return `
  Co metaforicky ovládal Unravelík svým pohybem po schodech?
  
  Odpověď na tuto otázku je i odpovědí na hádanku ve dveřích.
  
  ---
  Svou odpověď napište do terminálu. Pokud hádanku uhodnete, budete moct pokračovat dál; v opačném případě
  se nic nestane.

  (a nebo se můžete nechat podat: stačí napsat do terminálu '626-řešení'.)

`;
        }

        if (this.commandMatches(command, 'cas')) {
            return `
  Dveře se pomalu se skřípěním otevřely. Unravelík koukal do tmavého majáku a snažil se rozeznat, co je uvnitř...
  Vypadalo to jako další schodiště, které vedlo nahoru do věže.
  Udělal jeden krok dovnitř.
  A v tom se začaly dít věci.
  
  Slunce se prudce zvedlo z obzoru a utíkalo po obloze, během pár sekund bylo přímo nad hlavou a začalo klesat na
  východ. Unravelík oknem na protější straně majáku viděl, jak klesá a nakonec padá za obzor. Byla tma.
  Ale měsíc dělal to samé, zvedl se na jedné straně a hrozně rychle se pohyboval po obloze, stejně jako hvězdy.
  Bylo to jako by se Unravelík ocitl v nějakém špatném planetáriu a někdo k ovládání pustil bandu malých dětí.
  Skoro se mu začalo dělat špatně. Rozběhl se po schodech nahoru. Tohle nemůže být jen tak, říkal si, nahoře určitě
  najdu nějaké odpovědi!
  
  Celé mu to připadalo jako věčnost, slunce i měsíc musely padnout za obzor ve svém závodu alespoň pětkrát, ale
  nakonec se Unravelíkovi podařilo doběhnout nahoru.
  Zadýchaně se zastavil.
  A všiml si zvláštní věci. Obloha venku, kterou teď už viděl z ochozu majáku, se taky zastavila. Ale když se zkusil
  byť jen mírně pohnout... Tak se hýbala s ním.
  Tohle je tedy pořádně podezřelé, říkal si pořád, zatímco si dával pozor, aby dělal co nejméne zbytečných pohybů.
  Pomalu se otočil a stanul před dalšími dveřmi s podobným mechanismem jako u vstupu.
  
  ---
  679 - Přečíst nápis na dveřích.

`;
        }

        if (command === '679-hint') {
            return `
  Všichni žijeme ve více časových rovinách.
  Tou první je naše minulost.
  Tou druhou je naše přítomnost.
  A tou třetí je...
  
  ... Odpověď na tuto hádanku.
  
  (inu, vymýšlet nápovědy k hádankám nikdy nebyla naše silná stránka. ¯\\_(*-*)_/¯ )
  
  ---
  Svou odpověď napište do terminálu. Pokud hádanku uhodnete, budete moct pokračovat dál; v opačném případě
  se nic nestane.

  (a nebo se můžete nechat poddat: stačí napsat do terminálu '679-řešení'.)

`;
        }

        if (this.commandMatches(command, 'budoucnost')) {
            return `
  Dveře se po vyřčení správné odpovědi prudce otevřely a na Unravelíka tam čekala zvláštní věc. Vypadalo to trochu
  jako starodávná ponorka, ale... Co by tady na vrcholku majáku dělala ponorka?
  Inu, co naplat. Vlezl dovnitř a zavřel za sebou dveře. Tohle místo už je celé tak divné, že nemá smysl vůbec
  přemýšlet o tom, jaký to dává smysl.
  Chvíli seděl, zatímco venku se podle všeho nic nedělo.
  Pak se nebe začalo opět pomalu hýbat. A stále zrychlovalo.
  Nakonec už nebylo vidět ani jednotlivé hvězdy, byly to spíš šmouhy na obloze.
  Unravelík to užasle sledoval.
  
  V tu chvíli se světlo majáku rozzářilo oslepujícím světlem. Ozval se zvuk troubení obrovské lodi a Unravelík měl
  pocit, jako by ho slyšel nejen venku, ale i ve vlastní hlavě.
  Při pohledu ven bylo vidět, jak se na moři zhmotňují další a další majáky.
  Pak... se ponorka mírně vznesla. Unravelík se cítil, jako kdyby nic nevážil.
  A pak ho světlo zcela oslepilo a on zavřel oči.
  
  ---
  409 - Zjistit, co se stalo.

`;
        }

        if (command === '671') {
            return `
  Vylezl přes pár balvanů, přeskočil nízkou zídku a stanul pod tyčící se věží majáku. Zapadající slunce bylo pořád
  stejně vysoko, což už Unravelíkovi skoro ani nepřišlo divné.
  
  Ovšem ve chvíli, kdy začal stoupat po schodech ke dveřím majáku, se něco začalo dít.
  Světlo jako by se začalo měnit... Unravelík se znovu podíval směrem ke slunci. S každým schodem, o který vystoupal,
  se slunce blížilo k obzoru. A s každým schodem, který sestoupil, se od obzoru zase vzdálilo.
  To... Bylo skutečně zvláštní. Zdálo se, že tenhle maják - nebo přesněji Unravelíkova činnost okolo majáku - určovala
  pohyb času. Ale jakmile ze schodů sestoupil úplně, slunce se vrátilo do původní polohy nad obzorem.
  
  Unravelík nevěděl, co si počít. Nevěděl, jak se sem dostal; nevěděl, kde vlastně je; nevěděl ani, kolik je hodin,
  protože slunce přestalo fungovat normálně; a ještě ke všemu měl na dnešek nějaký program, a tohle ho od něj úplně
  rozptýlilo.
  Takhle by to nešlo, řekl si. Teď půjde do toho majáku, slunce neslunce, a zjistí co a jak.
  
  Unravelík tedy vystoupal po schodech až ke vstupu do majáku. Slunce bylo přesně napůl schované za obzorem.
  
  Na dveřích bylo něco napsané.
  
  626 - Přečíst nápis na dveřích.

`;
        }

        if (command === '681') {
            return `
  Unravelík se zhluboka nadechl a přistoupil k přednímu vchodu.
  Zaklepal a hlasitě pozdravil - byl tak ostatně naučený od babičky, která občas hůř slyšela a snadno se vylekala,
  takže se raději vždycky ohlásil hlasitěji, aby o něm věděla a neměla pocit, že se k ní někdo potají připlížil.
  Chvíli čekal... Ale z domu se neozval žádný zvuk. Dokonce ani trapné "Tady nikdo není," které by Unravelíkovi
  nahnalo husí kůži... No, možná vlastně dobře, že se nikdo neozval, pomyslel si.
  Rozhlédl se po verandě a rámu dveří. Zajímavé bylo, že tyhle dveře vypadaly narozdíl od všeho ostatního docela
  používaně. Nebyl na nich prach ani se z nich neodlupovala barva.
  Unravelík zkusmo sáhl na vrchní stranu rámu... A nahmatal tam kus nějakého papíru!
  Co s ním bude dělat?
  
  ---
  262 - Podívá se, co je na něm napsáno.
  405 - Nechá papír tam kde je a spíš se půjde podívat okolo domu.

`;
        }

        if (command === '706') {
            return `
  ...
  
  Unravelík z toho byl skutečně perplex. Otočil se kolem dokola.
  
  ...
  
  Všude byl písek. Akorát kousek za ním byl jeden malý seschlý stromek.
  
  Zkusil jít k moři a sáhnout rukou do vody.
  
  ...
  
  Opravdu se mu to nezdálo, byl u moře.
  Ale... jak? Jak se tohle stalo? Vždyť v jednu chvíli byl v lese, na louce a přeskakoval potůčky... A najednou
  je tady?
  Nemluvě o tom, že jediné moře, o kterém věděl že by bylo možné se k němu po několika dnech rychlého cestování
  dostat, bylo na opačnou stranu než kam ho vedla stezka, jestli je jeho orientační smysl co k čemu.
  
  Ještě jednou se bezradně rozhlédl.
  
  ---
  921 - Vydá se po pláži doleva.
  228 - Vydá se po pláži doprava.
  740 - Zůstane ještě chvíli na místě. Třeba je to jenom zvláštní sen a za chvilku se z něj probudí...?

`;
        }

        if (command === '734') {
            return `
  Unravelík si jenom pozvdechl, co je to dneska za lidi, oprášil si kalhoty a pomalu zase vyrazil.
  Beztak to je ale divné, vidět tady někoho běhat, to se opravdu musí dít něco mimořádného. Co si Unravelík
  pamatoval, tak tady vždy potkával jenom zamyšlené, pomalu chodící lidi. Žádné takové neomalené osoby, které
  kolem vás proběhnou, vyděsí vás a ještě vás porazí na zem!
  
  Takové myšlenky se mu honily hlavou, až nakonec dorazil až doprostřed rozvalin na hlavní náměstí. To už byl
  zase téměř tak klidný jako předtím a se zájmem se rozhlížel, jestli někoho neuvidí.
  Postavy tam nebyly žádné, aspoň tam kam měl Unravelík výhled. Vysoké, poměrně zachovalé budovy vytvářely moc
  zajímavý pohled a probouzely v člověku pocit, že se nachází na velmi významném místě.
  Byla tam ale jedna zvláštní věc, které si Unravelík všiml až po chvíli...
  Jedny postranní dveře do nejvyššího domu na celém náměstí byly vyplněné jakousi šedou mlhou.
  
  Co by to mohlo znamenat?
  
  No... Když se nad tím Unravelík zamyslel... Tohle bylo přece to, co Antické rozvaliny vždycky dělaly. ČLověk
  měl pocit, že do nich jde za něčím, a pak se stalo něco zcela neočekávaného, co ho často úplně rozhodilo.
  Ale nakonec, když se nechal unášet proudem událostí, zjistil, že se dostal přesně k tomu, za čím původně přišel.
  
  ---
  617 - Takže asi dává smysl, aby vstoupil dovnitř, že? A uvidí, co se stane dál...

`;
        }

        if (command === '740') {
            return `
  Jak tak Unravelík seděl a přemýšlel, postupně se mu podařilo si vybavit zvuk moře, který slýchával z Vykotlaného
  stromu. Tohle skutečně působilo, jako že to je to stejné moře... Ale jak je možné, že se ocitl přímo u něj? Vždyť
  ten zvuk měl být jenom zvuk... Nebo se snad opravdu v dutině stromu nacházel celý malý svět?
  Ale to je přece nesmysl... Nebo o něčem takovém Unravelík alespoň nikdy neslyšel.
  Dobře, tak řekněme, že se tedy skutečně dostal nějakým způsobem na místo, které je zdrojem zvuku Vykotlaného
  stromu.
  Pokud se dostal sem, musí existovat i cesta zpátky.
  Ale kudy by tak mohla vést? Unravelík se znovu podíval doleva a doprava, jestli neuvidí alespoň něco... Ale ani
  na jedné straně nic nebylo, kromě pláže a mírného kopce vedoucího někam do nitra ostrova.
  Zadíval se přímo před sebe... Pořád tam bylo zapadající slunce. Skoro to vypadalo, jako by se vůbec nepohnulo...
  Počkat.
  Ono se skutečně vůbec nehýbe. Unravelík už tam seděl nějakou chvíli, za tu dobu by si musel všimnout, že se poloha
  slunce aspoň trochu změnila!
  Dobře, pomyslel si. Něco tady pěkně nehraje, ale když se to vezme z té dobré stránky, aspoň by Unravelík neměl
  přijít o moc času v reálném světě, když bude tady a zkusí tomu přijít na kloub.
  Vstal, protáhl se a podíval se postupně doleva i doprava podél pláže.
  
  Nakonec nahmátl něco malého v kapse a rozhodl se, že si usnadní práci s vybíráním, kudy půjde:
  
  Když padne panna, tak půjde doleva.
  Když padne orel, půjde doprava.
  
  Pak vyhodil korunu do vzduchu.
  
  Co Unravelíkovi padlo?
  
  ---
  994 - Panna.
  240 - Orel.

`;
        }

        if (command === '779') {
            return `
  Obvykle bylo loučí směrem dovnitř do rozvalin více, ale tady v té části zřejmě dnes moc lidí nebylo, a tak
  Unravelík často šel úplnou tmou.
  To mu ale nevadilo, rozvaliny byly moc zajímavé za světla i za tmy.
  
  ...
  
  Co Unravelíka překvapilo, a možná mu to i trochu vadilo, byla skutečnost, že se v jedné chvíli z postranní uličky
  vyřítila postava a srazila ho k zemi.
  
  Unravelík se snažil vyhrabat na nohy, aby zjistil co to má být, ale ta postava už mizela ve tmě, sotva se mu
  podařilo zahlédnout červený záblesk... Asi nějakého kusu oblečení.
  Zavolal za ní, ale zpět se nic neozvalo.
  
  Poběží Unravelík za tajemnou postavou?
  
  ---
  975 - Rozběhne se za ní.
  734 - Ne, nebude se přece tolik namáhat. Raději bude volným krokem pokračovat do středu rozvalin.

`;
        }

        if (command === '812') {
            return `
  Unravelíkovi začal znít zvuk moře a větru intenzivně kolem celé hlavy. Měl pocit, jako by se nacházel uprostřed
  těch vln, jako by se přelévaly přímo přes něj...
  Byl to vážně zvláštní pocit. Dokonce se mu zdálo, že má od nich mokrou kůži...
  Zavřel oči, užíval si ten pocit, že ho moře zdánlivě unáší a on má pocit, že se téměř vznáší.
  Pak oči otevřel.
  
  Ale už nebyl u Vykotlaného stromu.
  Byl na mořské pláži.
  A vlny se přes něj skutečně přelévaly.
  
  Unravelík se zmateně postavil. Kolem nebylo široko daleko nic, jenom písčitá pláž, kousek od něj malý seschlý
  stromek a v dálce nad obzorem zapadající slunce.
  Unravelík neměl tušení, jak se tohle stalo. A když jsme u toho - "co" se vlastně stalo.
  Co bude dělat dál?
  
  ---
  921 - Vydá se po pláži doleva.
  228 - Vydá se po pláži doprava.
  740 - Zůstane ještě chvíli na místě. Třeba je to jenom zvláštní sen a za chvilku se z něj probudí...?

`;
        }

        if (command === '828') {
            return `
  Jak tak Unravelík seděl a přemýšlel, postupně se mu podařilo si vybavit zvuk moře, který slýchával z Vykotlaného
  stromu. Tohle skutečně působilo, jako že to je to stejné moře... Ale jak je možné, že se ocitl přímo u něj? Vždyť
  ten zvuk měl být jenom zvuk... Nebo se snad opravdu v dutině stromu nacházel celý malý svět?
  Ale to je přece nesmysl... Nebo o něčem takovém Unravelík alespoň nikdy neslyšel.
  Dobře, tak řekněme, že se tedy skutečně dostal nějakým způsobem na místo, které je zdrojem zvuku Vykotlaného stromu.
  Pokud se dostal sem, musí existovat i cesta zpátky.
  Ale kudy by tak mohla vést? Unravelík se znovu podíval doleva a doprava, jestli neuvidí alespoň něco... Ale ani na
  jedné straně nic nebylo, kromě pláže a mírného kopce vedoucího někam do nitra ostrova.
  Zadíval se přímo před sebe... Pořád tam bylo zapadající slunce. Skoro to vypadalo, jako by se vůbec nepohnulo...
  Počkat.
  Ono se skutečně vůbec nehýbe. Unravelík už tam seděl nějakou chvíli, za tu dobu by si musel všimnout, že se poloha
  slunce aspoň trochu změnila!
  Dobře, pomyslel si. Něco tady pěkně nehraje, ale když se to vezme z té dobré stránky, aspoň by Unravelík neměl
  přijít o moc času v reálném světě, když bude tady a zkusí tomu přijít na kloub.
  Vstal, protáhl se a podíval se postupně doleva i doprava podél pláže.
  
  Nakonec nahmátl něco malého v kapse a rozhodl se, že si usnadní práci s vybíráním, kudy půjde:
  
  Když padne panna, tak půjde doleva.
  Když padne orel, půjde doprava.
  
  Pak vyhodil korunu do vzduchu.
  
  Co Unravelíkovi padlo?
  
  ---
  994 - Panna.
  240 - Orel.

`;
        }

        if (command === '839') {
            return `
  Unravelík odpočíval jenom chvilku a potom se zvedl, aby se vydal za tou tajemnou postavou. Musela určitě jít na
  opačnou stranu od Unravelíka, a to znamenalo na jižní stranu lesa.
  Vydal se tedy tím směrem a snažil se všímat si všech nepatřičností, jako mezer v křoví, prošlapaných cestiček
  nebo čehokoli, co by mu napovědělo, kudy se vydat.
  A v tom ji uviděl! Byla schovaná za kmenem stromu, nenápadně vykukovala zpoza něj.
  Unravelík vyskočil a rozběhl se za ní.
  Jakmile to ta postava viděla, tak se otočila a začala utíkat.
  Unravelík zrychlil. Zkusil na ni zavolat, ať na něj počká, ale postava ani nezpomalila.
  Po malé chvilce náhle zmizela do křoví napravo od cesty. Unravelík běžel za ní, ale postava mu zase zmizela
  doslova před očima!
  Nicméně tentokrát Unravelíkovu pozornost upoutalo ještě něco jiného. Přímo před ním stála veliká dřevěná chalupa,
  kterou si nevybavoval.
  Měla temná okna, z okapů a střechy místy visel břečťan, dům měl spoustu věžiček a výstupků a na verandě bylo
  starodávné houpací křeslo... Všechno vypadalo, jako by to tady bylo odjakživa.
  A taky že v chalupě už dlouho nikdo nebydlel, působila celkově dost zanedbaně.
  
  Že by ta postava zmizela dovnitř?
  
  ---
  681 - Zkusit zaklepat na přední dveře.
  585 - Obejít chalupu kolem dokola, jestli nemá další vstup nebo něco jiného zajímavého.

`;
        }

        if (command === '856') {
            return `
  Unravelík se rozhodl jít do Antických rozvalin. Poznat sám sebe, to je přece moc důležité a jedině, když člověk
  zná sám sebe, tak může být šťastný a může pomáhat ostatním.
  Přemýšlel o tom, k jakému poznání tehdy došli staří Řekové... A kolik z něj do dnešní doby zmizelo a je nenávratně
  pryč. Sice toho známe spoustu, ale když si člověk představí, co všechno pravděpodobně neznáme...
  Vytváří to úplně novou perspektivu, pomyslel si. Jako by člověk byl jenom jedním z tisíců koleček hodinové věže,
  která ještě ke všemu ukazuje všechny časy lidstva najednou.
  
  Po cestě Unravelík potkával různé lidi. Všichni se mu zdáli, jako by většinu času hleděli do prázdna před sebou...
  Unravelík přemýšlel, co v tom prázdnu vidí. Možná si představují, co zrovna asi dělají jejich blízcí lidé?
  Asi ne. Na to si málokdo z nás v dnešní době vyhradí čas. Ale bylo by hezké, pomyslel si Unravelík, kdyby to
  dělali. Tak by vlastně byli svým lidem nablízku, i když by nebyli přímo s nimi.
  Po docela příjemné odpolední procházce po lesních cestách nakonec došel na rozcestí a zastavil se.
  
  ---
  936 - Bude pokračovat doleva, po cestě do Antických rozvalin?
  914 - Nebo se vydá doprava k Vykotlanému stromu na kopci, v němž se podle pověstí ozývá zvuk moře?

`;
        }

        if (command === '884') {
            return `
  Musel ujít ještě nemalý kus, než dorazil do středu rozvalin.
  Po cestě se už nic zvláštního nestalo, ačkoli Unravelík sledoval s napětím všechno, co se kolem šustlo.
  
  Obhlédl celé hlavní náměstí. Byla na něm vysoká budova, možná to původně býval nějaký kostel nebo radnice. Ale
  jinak nic neobvyklého nenašel...
  Až si nakonec všiml, že v postranním průchodu v té velké budově je zvláštní, neprůhledná šedá mlha.
  
  Možná by se dalo projít skrz? Koneckonců, nic jiného co by Unravelíka zaujalo tady není, a tohle je naopak pěkně
  podezřelé...
  
  ---
  617 - Zkusit projít šedou mlhou dovnitř.

`;
        }

        if (command === '889') {
            return `
  Unravelík si řekl, že už bylo dost brodění se pískem a vlnami, a půjde radši chvilku po cestě, i kdyby to mělo
  trvat déle. Cesta se mu docela líbila, byly podél ní zvláštní ostnaté rostliny, občas i nějaký kaktus a tak
  podobně.
  V jedné zatáčce ovšem narazil na docela nečekanou věc.
  Přímo uprostřed cesty stál opuštěný povoz. Kolem něj bylo pár pootevíraných beden, ale vypadalo to, že nikdo není
  v dohledu. Kdoví, jak dlouho tam ten povoz stojí...
  Unravelík přišel blíž, aby se na povoz podíval. Nebylo na něm nic zvláštního, ani v bednách nebylo skoro nic,
  jenom zbytky nějakých látek, v nichž asi bylo něco zabalené, a jedna nakousnutá okurka.
  
  Už chtěl jenom zakroutit hlavou a pokračovat dál, když si všiml celkem malé obálky zastrčené mezi prkny povozu.
  
  ---
  674 - Otevřít obálku a podívat se, co je uvnitř.
  671 - Nechat obálku být a pokračovat dál.

`;
        }

        if (command === '903-hint') {
            return `
  Ten papír, který jste četli, zněl jako že ho musel psát někdo docela chytrý. Nebo aspoň znalý situace.
  Zkuste se zeptat na baru, jestli o tomhle něco nevědí.
  A dejte si tam něco k pití, to pivo se nevypije samo!

  Nicméně... Pokud si opravdu, opravdu nevíte rady... Můžete zkusit kliknout na poslední tečku v nadpisu.
  Třeba díky tomu najdete, co hledáte.
  
  A nebo samozřejmě vždycky můžete do okna terminálu napsat '903-řešení'. ;)

`;
        }

        if (command === '914') {
            return `
  U Vykotlaného stromu Unravelík trávil mnoho času, když byl malý a brávali ho tam rodiče na pikniky. Poslouchat
  moře, jak duní z hlubin stromu byl moc zvláštní pocit. Jako by bylo na dosah, stačilo se jen natáhnout a pocítit
  písek na pláži a vlny přílivu, nebo jen nastavit tvář mořskému větru...
  Nikdy to ale nebylo doopravdy. Přirozeně, byl to koneckonců jenom Vykotlaný strom a zvláštní zvuky v jeho
  hlubinách vznikaly podobně jako vznikají v mušlích, když si je člověk přiloží k uchu. Ty taky vždy znějí, jako
  by se v nich ukrýval celý malý svět.
  Jak tak Unravelík přemýšlel a vzpomínal, cesta mu rychle utekla, skoro si ani nevšiml, že už dorazil ke kopci
  s Vykotlaným stromem. Zastavil se, aby se lépe rozhlédl po krajině.
  Vypadalo to, že všechno je přesně tak jak si pamatoval: okraj lesa, ze kterého se najednou vystoupí do slunečního
  svitu zalévajícího louku plnou různého kvítí, a přímo na vrcholku kopce stojí jediný strom s vykotlaným kmenem
  a pozůstatky po pořádně široké koruně. Je slyšet zpěv ptáků, výjimečně u stromu šlo vidět i nějaká další zvířátka,
  jako zajíce nebo srnky.
  Jednou tam Unravelík potkal ježka Káju, ale to bylo poněkud specifické setkání... Ježek Kája se ho lekl a vynadal
  Unravelíkovi, proč ho tak děsí, z čehož potom vznikla trapná situace, když se ukázalo, že tam čeká na svou milou
  a je nervózní z rande, na které mají společně jít. Unravelík si s ním o tom trochu promluvil a ujistil ho, že mu
  bude držet palce, aby všechno vyšlo hezky a rande bylo příjemné. Docela nedávno se Unravelík doslechl o tom, že
  ježek Kája a jeho milá spolu pořád žijí a začínají plánovat rodinu, což Unravelíka potěšilo. Asi i proto, že je
  znovu tady u Vykotlaného stromu, si na Káju vzpomněl.
  Inu, vydal se tedy po pěšině nahoru do kopce, až nakonec stanul přímo u stromu. Rád by si zase poslechl moře,
  jako to dělával, když byl malý...
  Co Unravelík udělá?
  
  ---
  402 - Nakloní se ke stromu a bude tiše poslouchat jedním uchem.
  812 - Zkusí do jedné zvlášť velké pukliny strčit celou hlavu, aby se zvukem moře obklopil.

`;
        }

        if (command === '921') {
            return `
  Unravelík šel, ani už nevěděl jak dlouho. Musela to být alespoň hodina chůze pískem a vlnami - zřejmě byl zrovna
  příliv. Bylo to ale vlastně docela příjemné, když se to tak vzalo.
  Zapadající slunce stále hřálo, ale už nespalovalo. A navíc vytvářelo krásné světlo, přesně takové, o kterém mluví
  všichni fotografové, když uděláte tu chybu a zeptáte se jich.
  Unravelík zrovna stoupal do kopce, písek tady zřejmě vytvořil jakousi dunu zvedající se napříč pláží, takže přes
  ni nebylo vidět.
  Nakonec stanul na vrcholu duny.
  A před sebou uviděl maják! Vysoký, červenobílý, na kamenitém výběžku, zkrátka přesně jak z knížek.
  Pořádně si oddechl, protože kde byla civilizace, tam museli být lidé. A kde byli lidé, odtamtud se muselo jít
  dostat nějakými konvenčními cestami.
  
  Kudy se Unravelík k majáku vydá?
  
  ---
  128 - Nejkratší cestou přímo podél pláže.
  889 - Po cestě uhýbající trochu do vnitrozemí - přímo na ní totiž stojí ukazatel s nápisem "K MAJÁKU".

`;
        }

        if (command === '936') {
            return `
  Unravelík šel dlouho. Věděl, že Antické rozvaliny jsou od jeho obydlí daleko, ale stejně ho překvapilo, že se
  skoro setmělo, než dorazil k pozůstatkům hradeb. A to je přitom teprve konec léta, žádná zima, kdy by dny byly
  krátké jako bývají!
  Ale nakonec si řekl, že to vlastně ničemu nevadí, má koneckonců sbaleno i na přespání pod širým nebem.
  Vždyť není kam spěchat. Ostatně při poznávání sebe sama by člověk spěchat ani neměl, protože právě tak mu může
  uniknout to nejdůležitější.
  Unravelík se tedy zhluboka nadechl, a pak se vydal do rozvalin.
  
  Netrvalo dlouho, než narazil na zvláštní věc. Přímo uprostřed malého náměstíčka pod rozkvetlým akátem byla
  knihovna, a plná knih!
  Vypadala, jako by tam stála odjakživa, ale to přitom nemohla být pravda... Unravelík přistoupil blíž a zkoumavě
  se na ni podíval. A po chvilce si všiml, že knihovna má na své vrchni straně něco napsané vybledlou křídou:
  
  "Najdi mě ve svém světě."
  
  Hmm. Tohle je poněkud zvláštní, řekl si Unravelík, vždyť já tady jsem ve svém světě. Ale možná přijde na to,
  co to znamená... Začal si tedy prohlížet hřbety knížek, jestli mu některý nenapoví, jak pokračovat.
  A skutečně, zdálo se, jako by jedna z knížek do knihovny nepatřila. Když ji Unravelík vytáhl, aby se do ní podíval,
  tak se nepatrně, ale přesto znatelně zachvěla. A když ji otevřel, zjistil, že po okrajích má na některých stránkách
  napsané nějaké poznámky!
  Tohle nemůže být jen tak, pomyslel si. Posadil se ke stolku s lampou, který byl hned vedle knihovny.
  A začetl se do knihy... A do poznámek po okrajích stránek.
  
  ---
  936-hint - Pokud si nejste jistí, co dělat dál, napište tento příkaz.

`;
        }

        if (command === '936-hint') {
            return `
  Někde na Kačeří najdete knihovnu podobnou té, jakou popisuje příběh. V ní se ukrývá další postup příběhu -
  možná právě v těch poznámkách.

  ...

  ...

  ...

  Něco nám říká, že ty správné úryvky z knih v knihovně na Kačeří by mohly být uvedeny i pod příkazem 'hovory'.

  ...

  A nebo taky můžete zkusit napsat '936-řešení', pokud zrovna nemáte náladu na antické čtení, že ano. :)

`;
        }

        if (command === '959') {
            return `
  Unravelík běžel - postava kličkovala mezi zbytky budov tak, že Unravelík vždy stihl jenom sotva zahlédnout, kudy
  se vydala, aby ji mohl pronásledovat.
  Občas se pokoušel zavolat, ale nemělo to žádnou odezvu, snad jen že postava ještě zrychlovala.
  
  Unravelík jí skoro nestačil, a to je přitom docela sportovní typ! Nakonec se zadýchaně zastavil na křižovatce
  a bezradně se rozhlédl, protože nevěděl, kudy dál.
  Už se chtěl zklamaně otočit zpátky, když v tom si všiml sotva rozeznatelného obrysu ve stínu na druhé straně
  cesty, jak na něj gestikuluje a ukazuje, aby šel k ní.
  
  Vydá se Unravelík k postavě?
  
  434 - Ano, ale obezřetně!
  607 - Ne, vrátí se zpět na cestu, kterou měl původně v plánu jít.

`;
        }

        if (command === '970') {
            return `
  Unravelíkovi to vždy přišlo trochu podivné... Jak by vlastně mohla tahle cesta vést každého někam jinam? To by
  se přece musela nějak pohybovat... A to cesty, nakolik si byl Unravelík jistý, obvykle nedělají.
  A co teprve, kdyby po ní nedejbože šli třeba... dva lidi zároveň? Co by ta cesta dělala potom, aha? Leda že by
  ty dva cestovatele brala jako jednu skupinu, a tím pádem by je vedla na společné místo...
  
  Jak takhle Unravelík přemýšlel, čas mu rychle utíkal. Cesta ho docela bavila, protože vedla nejrůznějšími druhy
  krajiny. Chvíli byl v údolí mezi lesnatými kopci, za chvíli na louce, pak zase přeskakoval potok a opatrně
  našlapoval nějakou slatí... A tak podobně, opravdu se nenudil.
  Je tedy pravda, že zatím pořád nevěděl, kam to vlastně jde. Původně chtěl jít nejspíše do Antických rozvalin,
  protože o těch se říkalo, že člověku dávaly nejzajímavější nápady, a to on přece potřeboval, aby pomohl tomu
  zvířátku.
  Ale ono to nakonec nevadí, v nejhorším případě si zítra přivstane a zajde tam po snídani.
  Když už je teď tady, tak by stálo za to zjistit, kam ho ta cesta vlastně dovede. Začal si zase všímat svého okolí.
  
  A hned se dost překvapeně zarazil. Stál... u moře? Cože?
  
  ---
  706 - Zapřemýšlet, kam se to Unravelík dostal.

`;
        }

        if (command === '975') {
            return `
  Unravelík běžel - postava kličkovala mezi zbytky budov tak, že Unravelík vždy stihl jenom sotva zahlédnout, kudy
  se vydala, aby ji mohl pronásledovat.
  Občas se pokoušel zavolat, ale nemělo to žádnou odezvu, snad jen že postava ještě zrychlovala.
  
  Unravelík jí skoro nestačil, a to je přitom docela sportovní typ! Nakonec se zadýchaně zastavil na křižovatce a
  bezradně se rozhlédl, protože nevěděl, kudy dál.
  Už se chtěl zklamaně otočit zpátky, když v tom si všiml sotva rozeznatelného obrysu ve stínu na druhé straně
  cesty, jak na něj gestikuluje a ukazuje, aby šel k ní.
  
  Vydá se Unravelík k postavě?
  
  ---
  434 - Ano, ale obezřetně!
  607 - Ne, vrátí se zpět na cestu, kterou měl původně v plánu jít.

`;
        }

        if (command === '979') {
            return `
  Unravelík šel, ani už nevěděl jak dlouho. Musela to být alespoň hodina chůze pískem a vlnami - zřejmě byl zrovna
  příliv. Bylo to ale vlastně docela příjemné, když se to tak vzalo.
  Zapadající slunce stále hřálo, ale už nespalovalo. A navíc vytvářelo krásné světlo, přesně takové, o kterém mluví
  všichni fotografové, když uděláte tu chybu a zeptáte se jich.
  Unravelík zrovna stoupal do kopce, písek tady zřejmě vytvořil jakousi dunu zvedající se napříč pláží, takže přes
  ni nebylo vidět.
  Nakonec stanul na vrcholu duny.
  A před sebou uviděl maják! Vysoký, červenobílý, na kamenitém výběžku, zkrátka přesně jak z knížek.
  Pořádně si oddechl, protože kde byla civilizace, tam museli být lidé. A kde byli lidé, odtamtud se muselo jít
  dostat nějakými konvenčními cestami.
  
  Kudy se Unravelík k majáku vydá?
  
  ---
  128 - Nejkratší cestou přímo podél pláže.
  889 - Po cestě uhýbající trochu do vnitrozemí - přímo na ní totiž stojí ukazatel s nápisem "K MAJÁKU".

`;
        }

        if (command === '994') {
            return `
  Unravelík šel, ani už nevěděl jak dlouho. Musela to být alespoň hodina chůze pískem a vlnami - zřejmě byl zrovna
  příliv. Bylo to ale vlastně docela příjemné, když se to tak vzalo.
  Zapadající slunce stále hřálo, ale už nespalovalo. A navíc vytvářelo krásné světlo, přesně takové, o kterém mluví
  všichni fotografové, když uděláte tu chybu a zeptáte se jich.
  Unravelík zrovna stoupal do kopce, písek tady zřejmě vytvořil jakousi dunu zvedající se napříč pláží, takže přes
  ni nebylo vidět.
  Nakonec stanul na vrcholu duny.
  A před sebou uviděl maják! Vysoký, červenobílý, na kamenitém výběžku, zkrátka přesně jak z knížek.
  Pořádně si oddechl, protože kde byla civilizace, tam museli být lidé. A kde byli lidé, odtamtud se muselo jít
  dostat nějakými konvenčními cestami.
  
  Kudy se Unravelík k majáku vydá?
  
  ---
  128 - Nejkratší cestou přímo podél pláže.
  889 - Po cestě uhýbající trochu do vnitrozemí - přímo na ní totiž stojí ukazatel s nápisem "K MAJÁKU".

`;
        }

        if (command === '996') {
            return `
  Unravelík nechtěl chodit přímo dovnitř... Raději se procházel takhle po obvodu, mezi rozpadlými základy domů
  a mezi volně rostoucími stromy, kterých tady bylo daleko více.
  Protože už byla tma, tak nebylo vidět moc daleko, ale naštěstí tady Unravelík nebyl sám. Kolem rozvalin se vždy
  pohybovaly nejrůznější bytosti z lesa, které tu hledaly inspiraci nebo uvolnění od každodennosti.
  Díky nim tady na zdech a sloupech také plály pochodně poskytující dostatek světla k tomu, aby se člověk neztratil.
  Přímo tady na cestě ale nikoho neviděl, jsou to koneckonců docela rozlehlé rozvaliny.
  
  Jak se tak procházel, podíval se nad hlavu a uvědomil si, že je úplně jasná noc a hvězdy jsou krásně vidět. Našel
  si tedy příhodné travnaté místo kousek od cesty bez osvětlení a natáhl se tam na záda.
  Koukal se, jak hvězdy bez přestání svítí, poznával některá souhvězdí... A uvažoval dál o tom, kolik z těch hvězd
  má asi planety. A na kolika z těch planet žijí podobní lidé jako my.
  A že tam určitě je někdo, kdo právě v tuto chvíli taky leží na zádech a kouká se na hvězdy... A možná že kdyby
  měli štěstí, tak by se jejich pohledy mohly na okamžik setkat.
  Určitě by to byl někdo, kdo taky při koukání do nebe přemýšlí a možná právě taky chce pomoci někomu, kdo je smutný.
  To byla pro Unravelíka moc hezká myšlenka.
  Nějakou dobu si to ještě představoval a pak váhavě zamával na nebe a vyslal tichý pozdrav.
  Kdoví, třeba ti lidé z jiné planety dokážou přijímat myšlenky podobně jako my rádiový signál.
  Třeba to někomu udělá radost.
  
  Pomalu se zvedl, protáhl se a pokračoval v cestě kolem hradeb. 
  
  ---
  779 - Nakonec dorazil na místo, kde už nešlo jít dál, a tak pokračoval směrem dovnitř do rozvalin.

`;
        }

        if (command === '7931') {
            return `
  Unravelík u knihovny strávil nějaký čas - díky lampě tam bylo dost světla, a tak se mu četlo docela snadno.
  Říkal si, jak je to podivné, že tady vlastně je a tráví čas... trochu čtením, ale v podstatě hlavně čekáním.
  A jak tak čekal, tak mu hlavou šly myšlenky.
  
  Napadlo ho, že tak ale tráví čas jistě každý. Často čekáme, než se Něco stane, na milníky v každodenním životě,
  které nám pomáhají udržet směr.
  Také může být těžké takhle pokračovat, když žádný milník nepřichází. Můžeme mít pocit, že už nás nic nečeká,
  a že věci nemají smysl. Co potom?
  No, v podstatě se dají dělat dvě věci. Buď se obrátit dovnitř, do svého nitra a hledat hloubku tam... A nebo se
  obrátit ven, a hledat nové lidi a kontakt s nimi.
  
  Obojí může fungovat, jsme každý jiný a v různých etapách života můžeme hledat něco jiného. Jen je potřeba si
  uvědomit, že je v pořádku hledat.
  Zároveň bychom neměli pro samé hledání ztratit ty věci, které už jsme našli. To Unravelíkovi přišlo ohromně důležité.
  Koneckonců, proč vůbec hledáme? Abychom to Něco našli. A aby nám to nakonec zůstalo a my jsme se o to mohli opřít a
  získat ve svém životě jistotu. Ať už je to cokoli.
  Nicméně je dobré vědět, že opravdu "najít" je docela vzácné. Na druhou stranu "hledat" můžeme pořád, a i to si můžeme
  užívat. Často je to nakonec i zajímavější, než když to Něco už máme.
  A úplně nejsložitější na tom všem je poznat, kdy už není třeba dál hledat.
  
  Věci se mění a nakonec o mnoho z toho, co jsme našli, můžeme přijít. Z různých důvodů, a přitom jen málo z nich bývá
  myšleno zle.
  Ale i když o ty naše věci přijdeme, tak to neznamená, že nemůžeme najít nové. Nemluvě o tom, že to co vnímáme jako
  Ztrátu, může někdy být jednoduše Změna...
  A Změna je veskrze dobrá věc, alespoň dokud neomezuje, nýbrž rozšiřuje možnosti.
  
  Unravelík si četl dlouho. Nakonec ale dostal pocit, že už ví, co by měl dělat dál.
  Když si toho pocitu všiml, tak se zamyslel, odkud asi přišel. Že by ten někdo, kdo měl najít tuhle knihovnu ve svém
  světě, ji už našel a ona mu poradila? Možná.
  Tak či tak, je čas vyrazit zase dál.
  
  Zvedl se od stolku, vrátil knížky do knihovny, ještě si vychutnal poslední okamžik přitomnosti zde, a pak vykročil.
  
  Kam Unravelík vyrazil?
  
  ---
  996 - Kolem rozvalin po vnitřním obvodu hradeb.
  616 - Hlouběji do centra rozvalin.
            
`;
        }

        // původně sem vedlo pouze '1989', upravil jsem to kvůli dopisu od táty dceři (viz příkaz 'figure_it_out')
        // if (command === '1989') {
        if (this.commandMatches(command, 'otazky') || command === 'T3Themt5=' || command === '1989') {
            return `
  Unravelíka vytrhlo z dřímání hlasité cvaknutí zámku. Vyskočil na nohy a vyrazil ke vchodu, aby se podíval, o co jde.
  Zámek na dveřích byl otevřený a on mohl vstoupit dovnitř! Nikdo ale nebyl v dohledu...
  
  Unravelík si prohlédl celou místnost. Byla to pěkně velká místnost, vedlo z ní spousta dveří do dalších chodeb a pokojů.
  Byla tam nepřirozeně hustá tma, měl docela problém nezakopnout o nábytek. Viděl spoustu obrazů, knihovny plné knih a
  také překvapivé množství zatuchlých perských koberců.
  Ten komu tahle chalupa patřila tedy neměl moc vkus pro praktické využití interiérů.
  
  Pak si Unravelík všiml nějakého zablikání na vršku schodiště vedoucího do druhého patra. Podíval se tam pozorněji a
  skutečně, svítilo tam nějaké namodralé světlo.
  Že by tam někdo byl? Hmm... Unravelík si řekl, že bude opatrný, ale zkusí zjistit, co se tam děje, protože ty vzkazy
  okolo domu a zvláštní zážitky celého dnešního dne... Možná že tady zjistí, co za tím vším je.
  
  Odhodlaně vystoupal po schodišti. Bylo už jasně vidět, že světlo vychází z pootevřených dveří na konci chodby.
  Opatrně se k nim připlížil, chvilku poslouchal, ale nic se neozývalo. Viděl škvírou jenom to mírně poblikávající
  namodralé světlo.
  
  ---
  871 - Zhluboka se nadechnout a vstoupit do místnosti.

`;
        }

        if (command === '871') {
            return `
  Asi dvě vteřiny se nedělo nic. Unravelík si stihl všimnout, že modré světlo vychází ze staré televize, která má na
  obrazovce pouze statické zrnění.
  Pak se seběhlo několik věcí najednou.
  
  Uslyšel, jak někdo volá jeho jméno. Otočil se tím směrem a viděl, jak na něj letí nějaké... zrcadlo? Chytil ho
  oběma rukama.
  Pak na něj ten někdo zakřičel, ať se otočí. Unravelík se nezmohl na odpor takovému naléhání, tak se otočil.
  A na okamžik se hrozně vyděsil! Letěla na něj nějaká hrozná černá postava s obrovskými pařáty, ale jak na ni
  nastavil zrcadlo, tak se s ohlušujícím jekotem stáhla někam do stínů. Vypadalo to, jako by ji to zrcadlo zapudilo!
  Unravelík ohromeně stál a nechápal, co se právě stalo. Pak mu došlo, že za ním přece někdo byl. Otočil se a spatřil
  tu postavu, jak klečí a objímá nějakou docela maličkou osobu ve žluté pláštěnce.
  Ta osoba bezhlasně poděkovala a pak udělala něco, čemu Unravelík už ale vůbec nedokázal porozumět.
  Dotkla se televizní obrazovky... A jak se jí dotkla, tak se zrnění začalo měnit, až tam začala být vidět nějaká
  chodba. A potom to tu osůbku vtáhlo dovnitř a najednou byla na té chodbě... v televizi!
  Unravelík už dávno přestal vnímat, že má dokořán otevřenou pusu a jenom zíral.
  
  Osoba, která na něj předtím křičela instrukce a teď zůstala tady s ním vstala, zamnula si ruce a otočila se k němu.
  Usmála se na něj a pokynula mu směrem ven.
  Unravelík stále jen užasle koukal a nevěděl, čí je. Nechal se vyvést ven před dům, který vypadal kupodivu pořád
  stejně, skoro jako by se v něm právě neudála hromada naprosto absurdních a neuvěřitelných věcí.
  Obrátil svůj zmatený pohled k osobě, která šla s ním.
  
  Představila se mu jako Unravelinka a vysvětlila, že to byla ona, kdo ho dnes tak trochu naháněl po lese. Potřebovala
  totiž jeho pomoc, ale byla si jistá, že by jí nechtěl pomoct jen tak, kdyby věděl, jaké nebezpečí u toho hrozí.
  Unravelík se chtěl nadechnout a hlesnout, jaké nebezpečí... Ale nějak ani to nedokázal, tak jenom odevzdaně poslouchal.
  Nakonec se tady ale sešli, což bylo moc dobře, protože nebýt Unravelíka, tak by ta osůbka ve žluté plášťence nemohla
  zpátky do svého světa. Byla tady totiž nedopatřením a tahle televize byla podle všeho jakýsi portál zpět.
  Problém byl, že spolu s tou osůbkou se sem do našeho světa dostala i ta příšera, která ji pronásledovala a Unravelinka
  potřebovala své kamarádce pomoct. Tím zrcadlem, které Unravelíkovi hodila, se jim podařilo zapudit tu příšeru...
  Ale celé je to na dlouhé povídání.
  
  Unravelinka se pomalu vydala pryč od chalupy. Unravelík šel za ní a říkal, že tedy přece... už teď snad mají čas,
  a jeho to moc zajímá, co se tam vlastně stalo!
  Tak pozval Unravelinku k sobě na návštěvu, udělá něco dobrého k pití a jídlu a můžou si to aspoň pořádně říct.
  Tak trochu by si zasloužil vysvětlení, a taky upřesnění, o jak velké nebezpečí vlastně šlo? A vůbec, co byl celý
  dnešní den zač, a ví o tom něco Unravelinka?
  
  Ona se jen usmála a přijala Unravelíkovo pozvání.
  Než zašli mezi stromy, tak ještě dodala, že se snad dostane i na nějakou pomoc tomu zvířátku, na které Unravelík
  dnes ráno myslel.
  
  ---
  1337 - dokončení hry.

`;
        }

        if (command === '1337') {
            return `
  Děkujeme, že jste si náš minigamebook zahráli!
  
  Naše svatba měla být na malou chvíli v některých věcech návratem do 20. století.
  A protože máme oba hodně rádi videohry, udělat hru ve stylu textových adventur z doby, kdy počítače ještě ani nutně
  neměly grafické rozhraní, ale pouze terminál, byl pro nás zajímavý kreativní experiment.
  
  Doufáme, že se vám líbil. :) A také nás zajímá, kolik odkazů na jiné videohry jste poznali, takže se nám klidně
  ozvěte, přijďte na návštěvu a můžeme si o tom všem povídat!
  
  M+J
  (a taky U+U)
  
  ...............................

`;
        }

      return 'Neznámý příkaz: ' + command;
    }

    commandMatches(command: string, matchValue: string) {
        return command.toLowerCase().replace('š', 's').replace('ř', 'r').replace('ž', 'z').replace('č', 'c').replace('ě', 'e').replace('ý', 'y').replace('á', 'a').replace('í', 'i').replace('é', 'e').replace('ů', 'u').replace('ú', 'u').localeCompare(matchValue, 'cs', { sensitivity: 'base' }) === 0;
    }

    openHref(href: string, scheduled: boolean = true): void {
        setTimeout(() => {
            return window.location.href = href;
        }, 10);
    }

}
