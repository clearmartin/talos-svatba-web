import { Component, ChangeDetectionStrategy, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Terminal } from 'primeng/terminal';

@Component({
    selector: 'p-terminal',
    template: `
        <div [ngClass]="'p-terminal p-component'" [ngStyle]="style" class="{{styleClass}}" (click)="focus(in)">
            <div *ngIf="welcomeMessage">{{welcomeMessage}}</div>
            <div class="p-terminal-content">
                <div *ngFor="let command of commands">
                    <span class="p-terminal-prompt">{{prompt}}</span>
                    <span class="p-terminal-command">{{command.text}}</span>
                    <div class="p-terminal-response">{{command.response}}</div>
                </div>
            </div>
            <div class="p-terminal-prompt-container">
                <span class="p-terminal-content-prompt">{{prompt}}</span>
                <input #in type="text" [(ngModel)]="command" class="p-terminal-input" autocomplete="off" (keydown)="handleCommand($event)" autofocus (blur)="focused = false" />
                <span class="p-terminal-content-command">{{ command }}</span>
                <span class="p-terminal-content-caret" [ngClass]="{'focused': focused}"></span>
            </div>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./terminal.css'],
    host: {
        class: 'p-element'
    }
})
export class PrimeNGTerminalPatched extends Terminal {

    @ViewChild('in') input!: ElementRef;

    focused: boolean = false;
    historyPointer: number = -1;
    commandDraft: string = '';

    override ngAfterViewInit() {
        super.ngAfterViewInit();
        this.input.nativeElement.addEventListener('keyup', (event: KeyboardEvent) => {
            if (event.key !== 'ArrowUp' && event.key !== 'ArrowDown') {
                this.commandDraft = this.command;
                this.historyPointer = this.commands.length;
                return;
            } else if (event.key === 'ArrowUp' && this.historyPointer > 0) { // up
                this.historyPointer--;
            } else if (event.key === 'ArrowDown') { // down
                if (this.historyPointer < this.commands.length - 1) {
                    this.historyPointer++;
                } else {
                    console.debug('setCommand - draft');
                    this.setCommand(this.commandDraft);
                    return;
                }
            }
            this.setCommand(this.commands[this.historyPointer].text);
        });
        setTimeout(() => {
            this.focus(this.input?.nativeElement);
        }, 1);
    }

    setCommand(val: string): void {
        console.log('setCommand:', val);
        this.command = val;
        this.cd.detectChanges();
        setTimeout(() => {
            const idx = this.command.length;
            this.input.nativeElement.setSelectionRange(idx, idx);
        }, 1);
    }

    override focus(element: HTMLElement) {
        super.focus(element);
        this.focused = true;
        this.cd.detectChanges();
    }

    focusInput(): void {
        this.focus(this.input?.nativeElement);
    }

    disable(): void {
        // no-op
    }

}
