import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: '[app-bios-boot]',
    templateUrl: './bios-boot.component.html',
    styleUrls: ['./bios-boot.component.scss'],
    preserveWhitespaces: true
})
export class BiosBootComponent implements OnInit {

    memoryIncrement: number = 64;

    showType: boolean = false;
    showLoad: boolean = false;
    showMessages: boolean = false;
    memory: string = '    0';

    @Output() bootFinished: EventEmitter<any> = new EventEmitter();

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        const self = this;
        setTimeout(() => {
            self.showType = true;
            setTimeout(() => {
                self.showLoad = true;
                self.loadMemory(0, () => {
                    setTimeout(() => {
                        self.showMessages = true;
                        setTimeout(() => {
                            self.bootFinished.emit();
                        }, 1500);
                    }, 100);
                });
            }, 100);
        }, 700);
    }

    loadMemory(num: number, callback: () => void): void {
        if (num >= 16384) {
            callback();
            return;
        }
        const newNum = num + this.memoryIncrement;
        this.memory = String(newNum).padStart(5, ' ');
        setTimeout(() => {
            this.loadMemory(newNum, callback);
        }, 5);
    }

}
