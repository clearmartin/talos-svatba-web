import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ScrollPanel } from 'primeng/scrollpanel';

@Component({
    selector: '[app-terminal-viewer]',
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.scss'],
    preserveWhitespaces: true
})
export class TerminalViewerComponent implements OnInit {

    @Input() imgUrl: string = '';
    @Input() qrCodeValue: string = '';
    @Input() imgUrlList: string[] = [];

    @ViewChild('panel') panel!: ScrollPanel;

    @Output() closeRequested: EventEmitter<void> = new EventEmitter();

    _content: string = '';

    @Input() set content(content: string) {
        if (!content) {
            this._content = '';
            return;
        }
        this._content = `<p>${content.replace(/\n/g, '</p><p>')}</p>`;
    }

    get content() {
        return this._content;
    }

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.focus();
        }, 1000);
    }

    focus() {
        //TODO make this scrollpanel focused!
        console.log('panel: ', this.panel);
        const textPane = this.panel.el.nativeElement.querySelector('.inner-pane');
        if (textPane !== null) {
            console.log('focusing: ', textPane);
            textPane.querySelector('p').click();
        }
    }

    closeClicked() {
        this.closeRequested.emit();
    }

}
