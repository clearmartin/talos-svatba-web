import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: '[app-static-info-pc]',
    templateUrl: './static-info-pc.component.html',
    styleUrls: ['./static-info-pc.component.scss'],
    preserveWhitespaces: true
})
export class StaticInfoPcComponent implements OnInit {

    ngOnInit(): void {}

    ngAfterViewInit(): void {
    }

}
