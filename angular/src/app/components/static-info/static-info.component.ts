import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: '[app-static-info]',
    templateUrl: './static-info.component.html',
    styleUrls: ['./static-info.component.scss'],
    preserveWhitespaces: true
})
export class StaticInfoComponent implements OnInit {

    ngOnInit(): void {}

    ngAfterViewInit(): void {
    }

}
