import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { StateService } from '../../service/state.service'

@Component({
    selector: '[app-login-section]',
    templateUrl: './login-section.component.html',
    styleUrls: ['./login-section.component.scss'],
    preserveWhitespaces: true
})
export class LoginSectionComponent implements OnInit {

    loginCode: string = '';
    showLoginInput: boolean = false;
    loginFailed: boolean = false;
    loginFailedMessages: string[] = [
        'kdepak',
        'zkoušej dál, kovboji',
        'samá voda',
        'hehe, ne',
        'helpline: 800 11[data_corrupted]',
        'to by tak hrálo',
        'nemyslím si',
    ]
    loginFailedMessage: string = '';

    @ViewChild('input') input!: ElementRef;

    constructor (public state: StateService) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this.state.loadCookieLoggedIn();
    }

    tryLogIn() {
        this.state.checkLoginCode(this.loginCode, () => {}, () => {
            this.loginFailed = true;
            this.loginFailedMessage = this.loginFailedMessages[Math.floor(Math.random() * this.loginFailedMessages.length)];
            setTimeout(() => {
                this.loginFailed = false;
                this.focusInput();
            }, 1000);
        });
    }

    focusInput() {
        setTimeout(() => {
            this.input.nativeElement.focus();
            this.input.nativeElement.setSelectionRange(0, this.loginCode.length);
        }, 10);
    }

}
