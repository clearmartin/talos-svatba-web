export enum Validation {
    REQUIRED = 'REQUIRED',
}

export class FormSpec {
    name: string = '';
    type: string = '';
    fields: FormSpecField[] = [];
}

export class FormSpecField {
    label: string = '';
    description: string = '';
    name: string = '';
    type: string = '';
    defaultValue: string = '';
    options: FormFieldOption[] = [];
    displayPattern: DisplayPattern | null = null;
    validations: Validation[] = [];
}

export class FormFieldOption {
    label: string = '';
    value: string = '';
    defaultChecked: boolean | null = false;
}

export enum PatternOperator {
    AND = 'AND',
    OR = 'OR',
}

export class DisplayPattern {
    operator: PatternOperator = PatternOperator.AND;
    items: DisplayPattern[] = [];
    fieldName: string = '';
    fieldValue: string = '';
}

export class SubmitData {
    name: string = '';
    typeName: string = '';
    fields: SubmitField[] = [];
    login: SubmitLogin = new SubmitLogin();
}

export class SubmitField {
    name: string = '';
    typeName: string = '';
    value: string = '';
}

export class SubmitLogin {
    name: string = '';
    email: string = '';
    loginCode: string | null = null;
}
