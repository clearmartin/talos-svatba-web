import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { NgForm, NgModel, FormControl, FormGroup, ControlContainer } from '@angular/forms';
import { Validation, FormSpecField, PatternOperator, DisplayPattern } from './form-model';
import { FormComponent } from './form.component';

@Component({
    selector: '[app-form-field]',
    templateUrl: './form-field.component.html',
    styleUrls: ['./form-field.component.scss'],
    viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ],
})
export class FormFieldComponent implements OnInit {

    eValidation = Validation;

    @Input() field!: FormSpecField;
    @Input() parentForm!: FormComponent;
    @Input() submitted: boolean = false;

    @ViewChild('fieldElem') fieldElem!: NgModel;
    @ViewChildren(NgModel) fields!: QueryList<NgModel>;

    required: boolean = false;
    controlsFields: FormFieldComponent[] = [];
    visible: boolean = true;

    constructor() { }

    ngOnInit(): void {
        this.required = this.field.validations.includes(Validation.REQUIRED);
    }

    ngAfterViewInit() {
        //this.evaluateDisplayPatternAndUse(true);
    }

    onChanged() {
        console.log('field changed1', this.fieldElem);
        console.log('field changed2', this.fields);

        const result = this.evaluateDisplayPattern();
        console.log('display:', result);

        console.log('value:', this.getValue());

        this.controlsFields.forEach(f => f.evaluateDisplayPatternAndUse());
    }

    addControlsField(field: FormFieldComponent) {
        if (!this.controlsFields.includes(field)) {
            console.log('addControlsField - to this:', this);
            console.log('addControlsField - add field:', field);
            this.controlsFields.push(field);
        }
    }

    evaluateDisplayPatternAndUse(register: boolean = false) {
        console.log('evaluateDisplayPatternAndUse');
        this.visible = this.evaluateDisplayPattern(null, register);
    }


    evaluateDisplayPattern(displayFormula: DisplayPattern | null = null, register: boolean = false): boolean {
        const fields: FormFieldComponent[] = this.parentForm.formFields.toArray() || [];
        const formula: DisplayPattern | null = displayFormula !== null ? displayFormula : (this.field.displayPattern || null);
        console.log('evaluateDisplayPattern - fields:', fields);
        console.log('evaluateDisplayPattern - formula:', formula);
        if (formula === null) {
            return true;
        }
        if (formula.fieldName) {
            const target: FormFieldComponent | undefined = fields.find(f => f.field.name === formula.fieldName);
            if (target === undefined) {
                console.log('error -> field not found by name:', formula.fieldName);
                return true;
            }
            if (register) {
                target.addControlsField(this);
            }
            return target.matchesValue(formula.fieldValue);
        }
        for (const item of formula.items) {
            const result = this.evaluateDisplayPattern(formula, register);
            if (result && formula.operator === PatternOperator.OR) {
                return true;
            }
            if (!result && formula.operator === PatternOperator.AND) {
                return false;
            }
        }
        return false;
    }

    matchesValue(expectedValue: string): boolean {
        console.log(`matchesValue - field=${this.field.name}, expected=${expectedValue}, actual=${this.getValue()}`);
        let val = expectedValue;
        const isNegated = val.startsWith('!');
        val = isNegated ? val.substring(1) : val;
        const isOneOfMany = val.startsWith('~');
        val = isOneOfMany ? val.substring(1) : val;
        const actualValues: string[] = this.getValue();
        const actualValue: string = actualValues.length <= 0 ? '' : (actualValues.length === 1) ? actualValues[0] : JSON.stringify(actualValues);
        let result = val === actualValue;
        if (!result && isOneOfMany) {
            result = actualValues.includes(val);
        }
        return isNegated ? !result : result;
    }

    getValue(): string[] {
        if (this.field.type === 'checkbox') {
            return this.fields.filter(f => f.value).map(f => f.name) || [];
        }
        const val = this.fieldElem.value === null ? '' : String(this.fieldElem.value);
        return [ val ];
    }

}
