import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { StateService } from '../../service/state.service'
import { NgForm, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FormSpec, FormSpecField, Validation, SubmitData, SubmitField, FormFieldOption, PatternOperator, DisplayPattern } from './form-model';
import { FormFieldComponent } from './form-field.component';


@Component({
    selector: '[app-form]',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    preserveWhitespaces: true
})
export class FormComponent implements OnInit {

    eValidation = Validation;

    @ViewChild('form') form!: NgForm;
    @ViewChildren(FormFieldComponent) formFields!: QueryList<FormFieldComponent>;

    spec: FormSpec | null = null;
    submitSuccess: boolean = false;
    submitFailure: boolean = false;
    submitting: boolean = false;

    constructor(private http: HttpClient,
                private changeDetectorRef: ChangeDetectorRef,
                private state: StateService) {
                    this.state.loggedInCode.subscribe((loginCode: string) => {
                        if (loginCode) {
                            this.loadPreviouslyFilledData();
                        }
                    });
                }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.loadData();
    }

    loadData() {
        this.http.get(`/assets/form-spec.json`, { responseType: 'json' }).subscribe(
            (res: any) => {
                console.log('form spec success - res:', res);
                this.spec = res;

                this.changeDetectorRef.detectChanges();

                this.formFields.forEach(f => f.evaluateDisplayPatternAndUse(true));

                this.loadPreviouslyFilledData();
            },
            (err: any) => {
                console.log('form spec error:', err);
            }
        );
    }

    loadPreviouslyFilledData() {
        if (!this.state.loggedInValue || this.spec === null) {
            return;
        }
        this.http.get(`/service/form/load?name=${this.spec.name}&typeName=${this.spec.type}&loginCode=${this.state.loggedInCodeValue}`).subscribe(
            (res: any) => {
                console.log('form load success - res:', res);

                const spec = this.spec!;
                this.spec = null;

                for (const field of res.fields) {
                    const specField = spec.fields.find(f => f.name === field.name);
                    if (specField !== undefined) {
                        specField.defaultValue = field.value;
                        if (specField.type === 'checkbox') {
                            const vals = this.deserializeValueSubmit(field.value);
                            specField.options.forEach(o => o.defaultChecked = vals.includes(o.value));
                        }
                    }
                }

                setTimeout(() => {
                    this.spec = spec;
                    this.changeDetectorRef.detectChanges();
                    this.formFields.forEach(f => f.evaluateDisplayPatternAndUse(true));
                }, 100);
            },
            (err: any) => {
                console.log('form load error:', err);
            }
        );
    }

    onSubmit() {
        console.log('submit form:', this.form);

        if (this.spec === null) {
            return;
        }

        this.submitFailure = false;

        if (this.form.form.status === 'INVALID') {
            return;
        }

        const submitData = new SubmitData();
        submitData.name = this.spec.name;
        submitData.typeName = this.spec.type;
        submitData.fields = this.formFields.map(f => {
            const submitField = new SubmitField();
            submitField.name = f.field.name;
            submitField.typeName = this.getDataFieldType(f.field);
            submitField.value = this.serializeValueSubmit(f.getValue());
            return submitField;
        });
        submitData.login.name = submitData.fields.find(f => f.name === 'nick')?.value || 'unknown';
        submitData.login.email = submitData.fields.find(f => f.name === 'email')?.value || 'unknown';
        submitData.login.loginCode = this.state.loggedInCodeValue;

        this.submitting = true;

        this.http.post(`/service/form/save`, submitData).subscribe(
            (res: any) => {
                console.log('form submit success - res:', res);
                this.submitSuccess = true;
                this.submitting = false;
                this.state.checkLoginCode(res.loginCode);
            },
            (err: any) => {
                console.log('form submit error:', err);
                this.submitFailure = true;
                this.submitting = false;
            }
        );
    }

    getDataFieldType(field: FormSpecField): string {
        if (field.type === 'checkbox') {
            if (field.options) {
                return 'multivalue';
            } else {
                return 'boolean';
            }
        }
        if (field.type === 'radio', field.type === 'selectbox') {
            return 'choice';
        }
        if (field.type === 'richtext') {
            return 'richtext';
        }
        return 'text';
    }

    serializeValueSubmit(vals: string[]): string {
        if (vals.length === 1) {
            return vals[0];
        }
        return JSON.stringify(vals);
    }

    deserializeValueSubmit(val: string): string[] {
        if (val.startsWith('[') && val.endsWith(']')) {
            return JSON.parse(val);
        }
        return [val];
    }

}
