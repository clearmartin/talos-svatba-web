import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { StateService } from '../../service/state.service'
import { NgForm, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FormSpec, FormSpecField, Validation, SubmitData, SubmitField, FormFieldOption, PatternOperator, DisplayPattern } from './form-model';


@Component({
    selector: '[app-form-view]',
    templateUrl: './form-view.component.html',
    styleUrls: ['./form-view.component.scss'],
    preserveWhitespaces: true
})
export class FormViewComponent implements OnInit {

    spec: FormSpec | null = null;
    data: SubmitData[] = [];

    listUrl: string = '';
    listExportUrl: string = '';

    constructor(private http: HttpClient,
                private changeDetectorRef: ChangeDetectorRef,
                private state: StateService) {
                    this.state.loggedInCode.subscribe((loginCode: string) => {
                        if (loginCode) {
                            this.loadData();
                        }
                    });
                }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
    }

    loadData() {

        this.http.get(`/assets/form-spec.json`, { responseType: 'json' }).subscribe(
            (spec: any) => {
                console.log('form spec success - res:', spec);
                this.spec = spec;

                const fieldNamesParam = (spec as FormSpec).fields.map(f => f.name).join(',');

                this.listUrl = `/service/form/list?name=${spec.name}&typeName=${spec.type}&loginCode=${this.state.loggedInCodeValue}`;
                this.listExportUrl = `${this.listUrl}&format=xlsx&orderedFieldNames=${fieldNamesParam}`;

                this.http.get(this.listUrl).subscribe(
                    (res: any) => {
                        console.log('form load success - res:', res);
                        this.data = res;
                    },
                    (err: any) => {
                        console.log('form load error:', err);
                    }
                );
            },
            (err: any) => {
                console.log('form spec error:', err);
            }
        );
    }

    getDataItemValue(dataItem: SubmitData, fieldName: string): string {
        return dataItem.fields.find(f => f.name === fieldName)?.value || '';
    }

}
