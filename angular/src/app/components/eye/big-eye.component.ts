import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: '[app-big-eye]',
    templateUrl: './big-eye.component.html',
    styleUrls: ['./big-eye.component.scss'],
    preserveWhitespaces: true
})
export class BigEyeComponent implements OnInit {

    @Input() eyeClassAppearance: string = '';

    eyeClassDisplay: string = '';
    shown: boolean = false;

    ngOnInit(): void {}

    ngAfterViewInit(): void {
    }

    public animateEye(animationArray: string[], timeout: number, callback: () => void = () => {}, repeat: boolean = true, animationArrayToRepeat: string[] = []): void {
        if (animationArray.length === 0) {
            console.log('animating ended - repeat?', repeat);
            console.log('animationArrayToRepeat:', animationArrayToRepeat);
            if (repeat && animationArrayToRepeat.length > 0) {
                this.animateEye(animationArrayToRepeat, timeout, callback, repeat, animationArrayToRepeat);
                return;
            }
            callback();
            return;
        }
        const arrayToRepeat = repeat && animationArrayToRepeat.length === 0 ? animationArray : animationArrayToRepeat;
        const finalTimeout = this.shown ? timeout : 1;
        const self = this;
        setTimeout(() => {
            const copiedArray = [...animationArray];
            const nextCls = copiedArray.shift() || '';
            this.shown = true;
            if (nextCls === 'blink') {
                self.animateBlink(() => {
                    self.animateEye(copiedArray, timeout, callback, repeat, arrayToRepeat);
                });
                return;
            }
            this.eyeClassDisplay = nextCls;
            this.animateEye(copiedArray, timeout, callback, repeat, arrayToRepeat);
        }, finalTimeout);
    }

    animateBlink(callback: () => void = () => {}): void {
        this.animateEye(['one', 'two', 'three', 'two', 'one'], 100, callback, false);
    }

    printout(pre: Element, callback: () => void = () => {}): void {
        //TODO
        const origText = pre.innerHTML;
        for (const line of origText.split('\n')) {
//             let preparedText =
        }
    }

}
