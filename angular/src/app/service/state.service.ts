import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

interface LoginResponse {
    login_code: string,
    name: string,
}

@Injectable()
export class StateService {

    public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public loggedInCode: BehaviorSubject<string> = new BehaviorSubject<string>('');
    public loggedInUsername: BehaviorSubject<string> = new BehaviorSubject<string>('');

    loggedInValue: boolean = false;
    loggedInCodeValue: string = '';
    loggedInUsernameValue: string = '';

    cookieName: string = 'loggedIn';

    loginUrl: string = '/service/user/login?login_code={loginCode}';

    constructor (private http: HttpClient) {
        this.loggedIn.subscribe((val: boolean) => this.loggedInValue = val);
        this.loggedInCode.subscribe((val: string) => this.loggedInCodeValue = val);
        this.loggedInUsername.subscribe((val: string) => this.loggedInUsernameValue = val);
//         this.loadCookieLoggedIn();
    }

    setLoggedIn(loggedIn: boolean): void {
        console.log('StateService: setting loggedIn=' + loggedIn);
        this.loggedIn.next(loggedIn);
    }

    setLoggedInCode(loggedInCode: string): void {
        console.log('StateService: setting loggedInCode=' + loggedInCode);
        this.loggedInCode.next(loggedInCode);
    }

    setLoggedInUsername(loggedInUsername: string): void {
        console.log('StateService: setting loggedInUsername=' + loggedInUsername);
        this.loggedInUsername.next(loggedInUsername);
    }

    logIn(loginCode: string, loginUsername: string): void {
        const data = btoa(encodeURIComponent(JSON.stringify(loginCode)));
        this.setCookieValue(this.cookieName, data);
        this.setLoggedIn(true);
        this.setLoggedInCode(loginCode);
        this.setLoggedInUsername(loginUsername);
    }

    logOut() {
        this.setCookieValue(this.cookieName, '');
        this.setLoggedIn(false);
        this.setLoggedInCode('');
        this.setLoggedInUsername('');

    }

    loadCookieLoggedIn() {
        const data = this.getCookieValue(this.cookieName);

        if (!data) {
            return;
        }

        const loginCode = JSON.parse(decodeURIComponent(atob(data)));
        console.log('loaded from cookie:', loginCode);

        this.checkLoginCode(loginCode);
    }

    getCookieValue(cookieName: string) {
        const result = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
        return result ? result.pop() : '';
    }

    setCookieValue(cookieName: string, cookieValue: string) {
        const exdate = new Date();
        exdate.setDate(exdate.getDate() + 365);
        document.cookie = `${cookieName}=${cookieValue}; expires=${exdate.toUTCString()}; path=/`;
    }

    checkLoginCode(loginCode: string, successCallback: () => void = () => {}, failureCallback: () => void = () => {}): void {
        console.log('checkLoginCode, loginCode=', loginCode);
        this.http.post(this.loginUrl.replace('{loginCode}', loginCode), null).subscribe(
            (res: any) => {
                console.log('checkLoginCode call success - res:', res);
                const loginRes: LoginResponse = res;
                this.logIn(loginRes.login_code, loginRes.name);
                successCallback();
            },
            (err: any) => {
                console.log('loginState call failed - res:', err);
                failureCallback();
            }
        );
    }

}
