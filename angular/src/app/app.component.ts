import { Component, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: '[app-root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    showHome: boolean = false;
    showQuestionnaire: boolean = false;
    showQuestionnaireData: boolean = false;
    showInfo: boolean = false;
    showInfoPc: boolean = false;
    showQrPozvanka: boolean = false;
	showWevpTv: boolean = false;
    showSong: boolean = false;
    showDance: boolean = false;

    enableCrtEffect: boolean = false;

    constructor(private detectorRef: ChangeDetectorRef) {}

    ngAfterViewInit() {

        if (window.location.pathname.startsWith('/dotaznik-data')) {
            this.showQuestionnaireData = true;
        } else if (window.location.pathname.startsWith('/dotaznik')) {
            this.showQuestionnaire = true;
        } else if (window.location.pathname.startsWith('/telefon')) {
            this.showInfo = true;
        } else if (window.location.pathname.startsWith('/info')) {
            this.showInfoPc = true;
        } else if (window.location.pathname.startsWith('/qr/pozvanka')) {
            this.showQrPozvanka = true;
		} else if (window.location.pathname.startsWith('/wevp-tv')) {
			this.showWevpTv = true;
        } else if (window.location.pathname.startsWith('/song')) {
			this.showSong = true;
        } else if (window.location.pathname.startsWith('/dance')) {
			this.showDance = true;
        } else {
            this.showHome = true;
            this.enableCrtEffect = true;
        }
        this.detectorRef.detectChanges();
    }
}
